#include <cstring>
#include "Lex.h"
#include <iostream>

Lex::Lex(type_of_lex t, long num, const char* str) {
	t_lex = t;
	v_int = num;
	if (str != nullptr) {
		v_str = new char[strlen(str) + 1];
		strcpy(v_str, str);
		len_lex = strlen(str);
	} else {
		v_str = nullptr;
		len_lex = 0;
	}
}

Lex::Lex(const Lex &lexem) {
	t_lex = lexem.t_lex;
	v_int = lexem.v_int;
	if (lexem.v_str != nullptr) {
		v_str = new char[lexem.len_lex + 1];
		strcpy(v_str, lexem.v_str);
	} else {
		v_str = nullptr;
	}
	len_lex = lexem.len_lex;
}

Lex::~Lex() {
	delete []v_str;
}

Lex& Lex::operator = (const Lex &lexem) {
	if (this == &lexem) {
		return *this;
	}
	t_lex = lexem.t_lex;
	v_int = lexem.v_int;
	if (v_str != nullptr) {
		delete []v_str;
	}
	if (lexem.v_str != nullptr) {
		v_str = new char[lexem.len_lex + 1];
		strcpy(v_str, lexem.v_str);
	} else {
		v_str = nullptr;
	}
	len_lex = lexem.len_lex;
	return *this;
} 

const type_of_lex Lex::get_type() const {
	return t_lex;
}

const char* Lex::get_str_value() const {
	return v_str;
}

const long Lex::get_int_value() const {
	return v_int;
}

const int Lex::get_lex_len() const {
	return len_lex;
}

ostream& operator << (ostream &s, Lex l) {
	s << '(' << l.t_lex << ", " << l.v_int << ", ";
	if (l.v_str != nullptr) {
		cout << l.v_str;
	} else {
		cout << "nullptr";
	}
	cout << ");";
	return s;
}
