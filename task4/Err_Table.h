#pragma once

#include "Error.h"

class Err_Table: public Error {
public:
	Err_Table() = default;
	Err_Table(const char* s);
	~Err_Table() = default;
	Err_Table(const Err_Table &e);
	Err_Table& operator = (const Err_Table &e);
	void cout_str();
};