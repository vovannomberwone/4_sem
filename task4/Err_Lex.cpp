#include "Err_Lex.h"
#include <iostream>

using namespace std;

Err_Lex::Err_Lex(const Err_Lex &e): Error(e) {
	;
}

Err_Lex::Err_Lex(const char* s) {
	putstr(s);
}

Err_Lex& Err_Lex::operator = (const Err_Lex &e) {
	putstr(e.getstr());
	return *this;
}

void Err_Lex::cout_str() {
	cout << "Error when make lexical analysis:\n" << getstr() << endl;
	return;
}
