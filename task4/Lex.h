#pragma once 

#include <iostream>

using namespace std;

enum type_of_lex {
	LEX_NULL,					/*0*/

	LEX_FROM,					/*1*/
	LEX_SELECT,					/*2*/
	LEX_UPDATE,					/*3*/
	LEX_INSERT,					/*4*/
	LEX_INTO,					/*5*/
	LEX_CREATE,					/*6*/
	LEX_TABLE,					/*7*/
	LEX_DELETE,					/*8*/
	LEX_DROP,					/*9*/
	LEX_SET,					/*10*/
	LEX_TEXT,					/*11*/
	LEX_LONG,					/*12*/
	LEX_WHERE,					/*13*/
	LEX_NOT,					/*14*/
	LEX_LIKE,					/*15*/
	LEX_IN,						/*16*/
	LEX_ALL,					/*17*/
	LEX_OR,						/*18*/
	LEX_AND,					/*19*/

	LEX_PLUS,					/*1*/	// '+'	//20
	LEX_DASH,					/*2*/	// '-'	//21
	LEX_ASTERISK,				/*3*/	// '*'	//22
	LEX_PERCENT,				/*4*/	// '%'	//23
	LEX_SOLIDUS,				/*5*/	// '/' 	//24
	LEX_COMMA,					/*6*/	// ','	//25
	LEX_SPACE,					/*7*/	// ' '	//26
	LEX_SINGLE_APOSTROPHE,		/*8*/	// '\''	//27
	LEX_LEFT_PARENTHESIS,		/*9*/	// '('	//28
	LEX_RIGHT_PARENTHESIS,		/*10*/	// ')'	//29
	LEX_EQUALS,					/*11*/	// '='	//30
	LEX_GREATER_THEN,			/*12*/	// '>'	//31
	LEX_LESS_THEN,				/*13*/	// '<'	//32
	LEX_GREATER_OR_EQUAL_THEN,	/*14*/	// '>='	//33
	LEX_LESS_OR_EQUAL_THEN,		/*15*/	// '<='	//34
	LEX_NOT_EQUAL,				/*16*/	// '!='	//35
	LEX_LEFT_SQUARE_BRACKET,	/*17*/	// '['	//36
	LEX_RIGHT_SQUARE_BRACKET,	/*18*/	// ']'	//37
	LEX_NEL, 					/*19*/  // '\n'	//38
	
	LEX_ID,						/**/			//39
	LEX_NUM,					/**/			//40
	LEX_STR,					/**/			//41
	LEX_FIN, 					/**/			//42
	LEX_BOOL,					/**/			//43
	LEX_ID_NUM,					/**/			//44
	LEX_ID_STR					/**/			//45
};

class Lex {
private:
	type_of_lex t_lex;
	long v_int;
	int len_lex;
	char *v_str;
public:
	Lex(type_of_lex t = LEX_NULL, long num = 0, const char* str = nullptr);
	Lex(const Lex &lexem);
	~Lex();
	const type_of_lex get_type() const ;
	const char* get_str_value() const ;
	const long get_int_value() const ;
	const int get_lex_len() const ;
	Lex& operator = (const Lex &l); 
	friend ostream& operator << (ostream &s, Lex l);
};