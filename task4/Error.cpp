#include "Error.h"
#include <cstring>
#include <iostream>

using namespace std;

Error::Error(const Error &e) {
	strcpy(str, e.str);
}

Error& Error::operator = (const Error &e) {
	strcpy(str, e.str);
	return *this;
}

int Error::putstr(const char *s) {
	if (strlen(s) > mlen_str) {
		return -1;
	} else {
		strcpy(str, s);
	}
	return 0;
}

const char* Error::getstr() const {
	return str;
}