#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <cstring>
#include "Err_Lex.h"
#include "Err_Table.h"
#include "Err_Synt.h"
#include "Err_Executer.h"
#include "TableOP.h"
#include "SQL_Client.h"
#include <iostream>

using namespace std;

int SQL_Client::msglen() {
	int len = 0;

	while (buf[len] != '\n') {
		len++;
	}
	return len;
}

void SQL_Client::save_file() {
	int file_d;
	char symb;

	file_d = open("sel_table_res", O_WRONLY | O_CREAT, 0666);
	for (long j = 0; j < len; j++) {
		read(fd, &symb, 1);
		write(file_d, &symb, 1);
	}
	close(file_d);
	return;
}

int SQL_Client::readmsg(){
	int len_msg = 0;
	char c;

	for ( ; len_msg < msg_size - 1; len_msg++) {
		if (read(0, &c, 1) <= 0) {
			return 0;
		}
		buf[len_msg] = c;
		if (c == '\n') {
			return 0;
		}
		if (len_msg == msg_size - 1) {
			return -1;
		}
	}
	return 0;
}

void handler(int signal) {
	if (signal == SIGUSR1) {
		throw "end of working";
	}
	return;
}

SQL_Client::SQL_Client() {
	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("client: socket");
		throw "client: socket";
	}
	sin.sin_family = AF_INET;

	printf("Enter port\n");
	port = 0;
	while (1) {
		while (1) {
			if (read(0, &c, 1) <= 0) {
				throw "cant read port";
			}
			if (c <= '9' && c >= '0') {
				port = port * 10 + c;
				if (port < 0) {
					throw "too long port";
				}
			} else if (c == '\n') {
				if (port > 0) {
					break;
				} else {
					throw "empty port";
				}
			} else {
				throw "expected Enter after port";
			}
		}
		if (!(port >= port_min && port <= port_max)) {
			printf("Enter port from %d to %d\n", port_min, port_max);
		} else {
			break;
		}
	}
	sin.sin_port = htons(port);
	gethostname(hostname, sizeof(hostname));
	if ((hp = gethostbyname(hostname)) == NULL) {
		fprintf(stderr, "client: %s: unknown host\n", hostname);
		throw "client: unknown host";
	}
	memcpy(&sin.sin_addr, hp->h_addr, hp->h_length);
 	if (connect(fd, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
 		perror("client: connect");
 		throw "client: connect";
 	}	
 	signal(SIGUSR1, handler);
	if ((pid = fork()) > 0) {
		close(0);
		while (1) {
			if (read(fd, &len, sizeof(len)) <= 0) {
				close(fd);
				kill(SIGUSR1, pid);
				throw "client: cant read or end session";
			}
			if (len > 0) {
				save_file();
				TableOP table;
				table.Get_Info("sel_table_res", FALSE);
				table.cout_table();
			} else {
				if (read(fd, &len, sizeof(len)) <= 0) {
					close(fd);
					kill(SIGUSR1, pid);
					throw "client: cant read";
				}
				if (read(fd, buf, len) <= 0) {
					close(fd);
					kill(SIGUSR1, pid);
					throw "client: cant read";
				}
				printf("%s\n", buf);
				kill(SIGUSR1, pid);
				throw "client: bad request";
			}
		}
	} else {
		while (1) {
			if (readmsg() < 0) {
				close(fd);
				kill(SIGUSR1, getppid());
				throw "client: cant read";
			}
			write(fd, buf, msglen() + 1);
			buf[0] = '\n'; // to execute, after enter next request
			write(fd, buf, 1);
		}
	}
 	close(fd);
}