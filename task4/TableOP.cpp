#include "TableOP.h"
#include "Table.h" 
#include "Err_Table.h"
#include <cstring>

TableOP:: TableOP() {
	is_open = FALSE;
	ct_rec = ct_field = 0;
}

TableOP::~TableOP() {
	if (is_open) {
		Close_Table(table_handle);
	}
}

void TableOP::Get_Info(const char* name, const bool &is_create_rqst) {
	if (is_open) {
		Close_Table(table_handle);
		ct_rec = ct_field = 0;
	}
	strcpy(table_name, name);
	if (!(is_create_rqst)) {
		Open_Table(name, &table_handle);
		Get_Fields_Num(table_handle, &ct_field);
		ct_rec = 0;
		Move_First(table_handle);
		while (!(After_Last(table_handle))) {
			ct_rec++;
			Move_Next(table_handle);
		}
		Move_First(table_handle);
		is_open = TRUE;		
	} else {
		is_open = FALSE;
	}
	return;
}

void TableOP::cout_table() {
	char *fname, *fstr;
	long fnum;
	FieldType ftype;

	cout << "----------------------------------------------" << endl;
	for (unsigned j = 0; j < ct_field; j++) {
		Get_Field_Name(table_handle, j, &fname);
		j != (ct_field - 1) ? cout << fname << '|' : cout << fname << endl;
	}

	Move_First(table_handle);
	for (unsigned int i = 0; i < ct_rec; i++) {
		for (unsigned j = 0; j < ct_field; j++) {
			Get_Field_Name(table_handle, j, &fname);
			Get_Field_Type(table_handle, fname, &ftype);
			if (ftype == Text) {
				Get_Text(get_handle(), fname, &fstr);
				j != (ct_field - 1) ? cout << fstr << '|' : cout << fstr << endl;
			}
			if (ftype == Long) {
				Get_Long(get_handle(), fname, &fnum);
				j != (ct_field - 1) ? cout << fnum << '|' : cout << fnum << endl;
			}
		}
		Move_Next(table_handle);
		cout << endl;	
	}
	cout << "----------------------------------------------" << endl;
	return;
}

const THandle TableOP::get_handle() const {
	return table_handle;
}

const char* TableOP::get_table_name() {
	return table_name;
}

void TableOP::Create_Table(const char *tableName, struct TableStruct *tableStruct) {
	Errors err;

	err = createTable(tableName, tableStruct);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Delete_Table(const char *tableName) {
	Errors err;

	err = deleteTable(tableName);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Open_Table(const char *tableName, THandle *tableHandle) {
	Errors err;

	err = openTable(tableName, tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Close_Table(THandle tableHandle) {
	Errors err;

	err = closeTable(tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Move_First(THandle tableHandle) {
	Errors err;

	err = moveFirst(tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Move_Last(THandle tableHandle) {
	Errors err;

	err = moveLast(tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Move_Next(THandle tableHandle) {
	Errors err;

	err = moveNext(tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Move_Previos(THandle tableHandle) {
	Errors err;

	err = movePrevios(tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

bool TableOP::Before_First(THandle tableHandle) {
	return(beforeFirst(tableHandle));
}

bool TableOP::After_Last(THandle tableHandle) {
	return(afterLast(tableHandle));
}

void TableOP::Get_Text(const THandle tableHandle, const char *fieldName, char **pvalue) const {
	Errors err;

	err = getText(tableHandle, fieldName, pvalue);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Get_Long(const THandle tableHandle, const char *fieldName, long *pvalue) const {
	Errors err;

	err = getLong(tableHandle, fieldName, pvalue);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Start_Edit(THandle tableHandle) {	
	Errors err;

	err = startEdit(tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Put_Text(THandle tableHandle, const char *fieldName, const char *value) {
	Errors err;

	err = putText(tableHandle, fieldName, value);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Put_Long(THandle tableHandle, const char *fieldName, const long value) {
	Errors err;

	err = putLong(tableHandle, fieldName, value);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::TableOP::Finish_Edit(THandle tableHandle) {
	Errors err;

	err = finishEdit(tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Create_New(THandle tableHandle) {
	Errors err;

	err = createNew(tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Put_Text_New(THandle tableHandle, const char *fieldName, const char *value) {
	Errors err;

	err = putTextNew(tableHandle, fieldName, value);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Put_Long_New(THandle tableHandle, const char *fieldName, const long value) {
	Errors err;

	err = putLongNew(tableHandle, fieldName, value);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Insert_New(THandle tableHandle) {
	Errors err;

	err = insertNew(tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Inserta_New(THandle tableHandle) {
	Errors err;

	err = insertaNew(tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Insertz_New(THandle tableHandle) {
	Errors err;

	err = insertzNew(tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Delete_Rec(THandle tableHandle) {
	Errors err;

	err = deleteRec(tableHandle);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

const char* TableOP::Get_Error_String(enum Errors code) {
	return ErrorText[code];
}

void TableOP::Get_Field_Len(THandle tableHandle, char*fieldName, unsigned *plen) {
	Errors err;

	err = getFieldLen(tableHandle, fieldName, plen);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Get_Field_Type(THandle tableHandle, const char *fieldName, enum FieldType *ptype) {
	Errors err;

	err = getFieldType(tableHandle, fieldName, ptype);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Get_Fields_Num(THandle tableHandle, unsigned *pNum) {
	Errors err;

	err = getFieldsNum(tableHandle, pNum);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}

void TableOP::Get_Field_Name(THandle tableHandle, unsigned index, char **pFieldName) {
	Errors err;

	err = getFieldName(tableHandle, index, pFieldName);
	if (err != OK) {
		throw Err_Table(ErrorText[err]);
	}
	return;
}