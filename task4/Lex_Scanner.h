#pragma once

#include <vector>
#include "Err_Lex.h"
#include "Lex.h"

class Lex_Scanner {
private:
	static const int mlen_id = 80, mlen_str = 150;
	friend class Synt_Scanner;  // чтобы был доступ к TW[]
	Lex lexem;
	FILE *fp;
	char str[mlen_str + 1], buf[mlen_id + 1];
	int c, buf_top;
	static const char alphid[];
	static const char letters[];
	static const char dig[];
	static const char common_del[];
	static const char delims[];
	static const char* TW[];
	void gc();
	void clear_buf();
	void cut();
	void add();
	int instr(const char c, const char *str);
	void State_Ident_or_Slave(Lex &lexem);
	void State_Numb(Lex &lexem);
	void State_Com_Del(Lex &lexem);
	void State_GrLess(Lex &lexem);
	void State_Not(Lex &lexem);
	void State_Str(Lex &lexem);
public:
	Lex_Scanner(FILE* file_pointer = stdin);
	~Lex_Scanner();
	bool getlex(Lex &lexem);
	void Scan(); // считать и вывести программу во внутр представлении
};