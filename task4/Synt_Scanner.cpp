#include "Synt_Scanner.h"
#include "Err_Synt.h"
#include <stack>
#include <cstring>
#include <string>
#include "TableOP.h"
#include "SQL_Request.h"

using namespace std;

Synt_Scanner::Synt_Scanner(FILE *fp): empty_table(), rqst(), lexem(), lscanner(fp) {
	is_valid = FALSE;
}

Synt_Scanner::~Synt_Scanner() {
	;
}

bool Synt_Scanner::str_valid() {
	return is_valid;
}

void Synt_Scanner::gl() {
	is_end = lscanner.getlex(lexem);
	c_type = lexem.get_type();
	return;
}

SQL_Request Synt_Scanner::get_request() {
	return rqst;
}

void Synt_Scanner::miss() {
	spc_ct = 0;
	while (1) {
		gl();
		if (c_type != LEX_SPACE) {
			break;
		}
		spc_ct++;
	}
	return;
}

void Synt_Scanner::scan_request() {
	rqst.clear_request();
	is_valid = FALSE;
	miss();
	switch (c_type) {
	case LEX_SELECT:
		rqst.type = SELECT_REQUEST;
		State_Select();
		break;
	case LEX_INSERT:
		rqst.type = INSERT_REQUEST;
		State_Insert();
		break;
	case LEX_UPDATE:
		rqst.type = UPDATE_REQUEST;
		State_Update();
		break;
	case LEX_DELETE:
		rqst.type = DELETE_REQUEST;
		State_Delete();
		break;
	case LEX_CREATE:
		rqst.type = CREATE_REQUEST;
		State_Create();
		break;
	case LEX_DROP:
		rqst.type = DROP_REQUEST;
		State_Drop();
		break;
	case LEX_NEL:	// miss empty or only space request
		rqst.type = EMPTY_REQUEST;
		break;
	case LEX_FIN:
		rqst.type = FIN_REQUEST;
		break;
	default:
		throw Err_Synt("Expected one of 6 types of statement");
	}
	is_valid = TRUE;
	return;
}

void Synt_Scanner::scan_program() {
	int i = 1;
	do {
		scan_request();
		if (is_valid) {
			cout << "request " << i << " is correct" << endl;
			rqst.cout_request();
			cout << endl;
		} else {
			cout << "request " << i << " is not correct" << endl;
		}
		i++;
	} while (c_type != LEX_FIN);
	return;
}

void Synt_Scanner::scan_err_program() {
	int i = 1;
	do {
		try {
			scan_request();
		}
		catch (Err_Lex err) {
			err.cout_str();
		}
		catch (Err_Table err) {
			err.cout_str();
		}
		catch (Err_Synt err) {
			err.cout_str();
		}
		catch (...) {
			cout << "HERE" << endl;
		}
		if (is_valid) {
			cout << "request " << i << " is correct" << endl;
			rqst.cout_request();
			cout << endl;
		} else {
			cout << "request " << i << " is not correct" << endl;
			while (c_type != LEX_FIN && c_type != LEX_NEL) {
				gl();
			}
			cout << endl;
		}
		i++;
	} while (c_type != LEX_FIN);
	return;
}

void Synt_Scanner::det_lex(const char *s, const type_of_lex &t) {
	if (c_type != t) {
		if (!(LEX_FROM <= c_type && c_type <= LEX_FIN)) {
			throw Err_Synt("internal error with type of lexem");
		}
		strcpy(str, "Expected ");
		strcat(str, lscanner.TW[t]);
		strcat(str, " after ");
		strcat(str, s);
		strcat(str, " but ");
		strcat(str, lscanner.TW[c_type]);
		strcat(str, " detected");
		throw Err_Synt(str);
	}
	return;
}

void Synt_Scanner::State_Select() {
	gl();
	det_lex("SELECT", LEX_SPACE);
	miss();
	if (c_type == LEX_ASTERISK) {
		rqst.all_fields = TRUE;
		gl();
		det_lex("*", LEX_SPACE);
		miss();
	} else if (c_type == LEX_ID) {
		rqst.select_list.push_back(string(lexem.get_str_value()));
		while (1) {
			miss();
			if (c_type == LEX_COMMA) {
				miss();
				det_lex(",", LEX_ID);
				rqst.select_list.push_back(string(lexem.get_str_value()));
			} else {
				if (spc_ct == 0) {
					throw Err_Synt("Expected space before FROM");
				}
				break;
			}
		}
	} else {
		throw Err_Synt("Expected ID or * after SELECT");
	}
	if (rqst.same_tables_or_fields()) {
		throw (Err_Synt("same_tables_or_fields are identified"));
	}
	det_lex("field names", LEX_FROM);
	gl();
	det_lex("FROM", LEX_SPACE);
	miss();
	if (c_type != LEX_ID) {
		throw Err_Synt("Expected name of table in SELECT statement");
	}
	rqst.list_tables.push_back(empty_table);
	rqst.list_tables.back().Get_Info(lexem.get_str_value(), FALSE);
	while (1) {
		miss();
		if (c_type == LEX_COMMA) {
			miss();
			det_lex(",", LEX_ID);
			rqst.list_tables.push_back(empty_table);
			rqst.list_tables.back().Get_Info(lexem.get_str_value(), FALSE);
		} else {
			break;
		}
	}
	State_Where();
	if (is_end) {
		return;
	}
	miss();
	if (!is_end) {
		throw Err_Synt("expected space or EOF after SELECT request");
	}
	return;
}

void Synt_Scanner::State_Insert() {
	gl();
	det_lex("INSERT", LEX_SPACE);
	miss();
	det_lex("INSERT", LEX_INTO);
	gl();
	det_lex("INTO", LEX_SPACE);
	miss();
	det_lex("INTO", LEX_ID);
	rqst.list_tables.push_back(empty_table);
	rqst.list_tables.back().Get_Info(lexem.get_str_value(), FALSE);
	miss();
	det_lex("table name", LEX_LEFT_PARENTHESIS);
	miss();
	if (c_type == LEX_NUM) {
		rqst.insert_list.push_back(lexem);
	} else if (c_type == LEX_STR) {
		rqst.insert_list.push_back(lexem);
	} else {
		throw Err_Synt("Expected str or num in insert request");
	}
	while (1) {
		miss();
		if (c_type == LEX_COMMA) {
			miss();
			if (c_type == LEX_NUM) {
				rqst.insert_list.push_back(lexem);
			} else if (c_type == LEX_STR) {
				rqst.insert_list.push_back(lexem);
			} else {
				throw Err_Synt("Expected str or num in insert request");
			}
		} else {
			break;
		}
	}
	det_lex("values", LEX_RIGHT_PARENTHESIS);
	miss();
	if (!is_end) {
		throw Err_Synt("expected space or EOF after INSERT request");
	}
	return;
}

void Synt_Scanner::State_Update() {
	FieldType ptype;

	is_update_expr = TRUE;
	gl();
	det_lex("UPDATE", LEX_SPACE);
	miss();
	det_lex("UPDATE", LEX_ID);
	rqst.list_tables.push_back(empty_table);
	rqst.list_tables.back().Get_Info(lexem.get_str_value(), FALSE);
	gl();
	det_lex("table name", LEX_SPACE);
	miss();
	det_lex("table name", LEX_SET);
	gl();
	det_lex("SET", LEX_SPACE);
	miss();
	det_lex("SET", LEX_ID);
	strcpy(rqst.name_update, lexem.get_str_value());
	miss();
	det_lex("field name", LEX_EQUALS);
	miss();
	State_Expression();
	sem_pop();
	rqst.list_tables.begin()->Get_Field_Type(rqst.list_tables.begin()->get_handle(), rqst.name_update, &ptype);
	if ((sem_st_type == LEX_NUM || sem_st_type == LEX_ID_NUM) && ptype == Long) {
		;
	} else if ((sem_st_type == LEX_STR || sem_st_type != LEX_ID_STR) && ptype == Text) {
		;
	} else {
		throw Err_Synt("incorrect type of expression in update request");
	}
	is_update_expr = FALSE;
	State_Where();
	if (is_end) {
		return;
	}
	miss();
	if (!is_end) {
		throw Err_Synt("expected space or EOF after UPDATE request");
	}
	return;
}

void Synt_Scanner::State_Delete() {
	gl();
	det_lex("DELETE", LEX_SPACE);
	miss();
	det_lex("DELETE", LEX_FROM);
	gl();
	det_lex("FROM", LEX_SPACE);
	miss();
	det_lex("FROM", LEX_ID);
	rqst.list_tables.push_back(empty_table);
	rqst.list_tables.back().Get_Info(lexem.get_str_value(), FALSE);
	gl();
	det_lex("table name", LEX_SPACE);
	miss();
	State_Where();
	if (is_end) {
		return;
	}
	miss();
	if (!is_end) {
		throw Err_Synt("expected space or EOF after DELETE request");
	}
	return;
}

void Synt_Scanner::State_Create() {
	gl();
	det_lex("CREATE", LEX_SPACE);
	miss();
	det_lex("CREATE", LEX_TABLE);
	gl();
	det_lex("TABLE", LEX_SPACE);
	miss();
	det_lex("TABLE", LEX_ID);
	rqst.list_tables.push_back(empty_table);
	rqst.list_tables.back().Get_Info(lexem.get_str_value(), TRUE);
	miss();
	det_lex("table name", LEX_LEFT_PARENTHESIS);
	miss();
	det_lex("(", LEX_ID);
	strcpy(cur_field.name, lexem.get_str_value());
	gl();
	det_lex("field name", LEX_SPACE);
	miss();
	if (c_type == LEX_TEXT) {
		cur_field.type = Text;
		miss();
		det_lex("TEXT", LEX_LEFT_PARENTHESIS);
		miss();
		det_lex("(", LEX_NUM);
		if (lexem.get_int_value() > lscanner.mlen_id) {
			throw Err_Synt("cant create table because max len of text field exceeded");
		}
		cur_field.len = lexem.get_int_value();
		miss();
		det_lex("number", LEX_RIGHT_PARENTHESIS);
	} else if (c_type == LEX_LONG) {
		cur_field.type = Long;
		cur_field.len = sizeof(long);
	} else {
		throw Err_Synt("expected TEXT or LONG");
	}
	rqst.create_list.push_back(cur_field);
	while (1) {
		miss();
		if (c_type == LEX_COMMA) {
			miss();
			det_lex(",", LEX_ID);
			strcpy(cur_field.name, lexem.get_str_value());
			gl();
			det_lex("field name", LEX_SPACE);
			miss();
			if (c_type == LEX_TEXT) {
				cur_field.type = Text;
				miss();
				det_lex("TEXT", LEX_LEFT_PARENTHESIS);
				miss();
				det_lex("(", LEX_NUM);
				if (lexem.get_int_value() > lscanner.mlen_id) {
					throw Err_Synt("cant create table because max len of text field exceeded");
				}
				cur_field.len = lexem.get_int_value();
				miss();
				det_lex("number", LEX_RIGHT_PARENTHESIS);
			} else if (c_type == LEX_LONG) {
				cur_field.type = Long;
				cur_field.len = sizeof(long);
			} else {
				throw Err_Synt("expected TEXT or LONG");
			}
			rqst.create_list.push_back(cur_field);
		} else {
			break;
		}
	}
	det_lex("field definition", LEX_RIGHT_PARENTHESIS);
	miss();
	if (!is_end) {
		throw Err_Synt("expected space or EOF after CREATE request");
	}
	return;
}

void Synt_Scanner::State_Drop() {
	gl();
	det_lex("DROP", LEX_SPACE);
	miss();
	det_lex("DROP", LEX_TABLE);
	gl();
	det_lex("TABLE", LEX_SPACE);
	miss();
	det_lex("TABLE", LEX_ID);
	rqst.list_tables.push_back(empty_table);
	rqst.list_tables.back().Get_Info(lexem.get_str_value(), FALSE);
	miss();
	if (!is_end) {
		throw Err_Synt("expected space or EOF after DROP request");
	}
	return;
}

void Synt_Scanner::sem_pop() {
	sem_st_type = sem_stack.top();
	sem_stack.pop();
	return;
}

void Synt_Scanner::sem_push(const type_of_lex &t) {
	sem_stack.push(t);
	return;
}

void Synt_Scanner::lex_add(const Lex &l) {
	if (is_update_expr) {
		rqst.expr_update_list.push_back(l);
	} else {
		rqst.expr_list.push_back(l);
	}
	return;
}

void Synt_Scanner::State_Where() {
	det_lex("request", LEX_WHERE);
	gl();
	det_lex("WHERE", LEX_SPACE);
	miss();
	if (c_type == LEX_ALL) {
		rqst.where_all = TRUE;
		gl();
		return;
	} else {
		State_Expression();
		sem_pop();
		if (sem_st_type == LEX_BOOL) {
			return;
		}
		if (c_type == LEX_NOT) {
			rqst.is_not = TRUE;
			gl();
			det_lex("NOT", LEX_SPACE);
			miss();
			if (c_type == LEX_LIKE && sem_st_type == LEX_ID_STR) {
				State_Like();
			} else if (c_type == LEX_IN && (sem_st_type == LEX_NUM || sem_st_type == LEX_ID_NUM)) {
				is_str_list = FALSE;
				State_In();
			} else if (c_type == LEX_IN && (sem_st_type == LEX_STR || sem_st_type == LEX_ID_STR)) {
				is_str_list = TRUE;
				State_In();
			} else {
				throw Err_Synt("incorrect LIKE/IN part");
			}
		} else {
			if (c_type == LEX_LIKE && sem_st_type == LEX_ID_STR) {
				State_Like();
			}  else if (c_type == LEX_IN && (sem_st_type == LEX_NUM || sem_st_type == LEX_ID_NUM)) {
				is_str_list = FALSE;
				State_In();
			} else if (c_type == LEX_IN && (sem_st_type == LEX_STR || sem_st_type == LEX_ID_STR)) {
				is_str_list = TRUE;
				State_In();
			} else {
				throw Err_Synt("incorrect LIKE/IN part");
			}
		}
	}
	return;
}

void Synt_Scanner::State_Like() {
	rqst.is_like = TRUE;
	gl();
	det_lex("LIKE", LEX_SPACE);
	miss();
	det_lex("LIKE", LEX_STR);
	strcpy(rqst.str_like, lexem.get_str_value());
	gl();
	return;
} 

void Synt_Scanner::State_In() {
	rqst.is_in = TRUE;
	miss();
	det_lex("IN", LEX_LEFT_PARENTHESIS);
	miss();
	if (c_type == LEX_NUM && !(is_str_list)) {
		rqst.const_num_list.push_back(lexem.get_int_value());
		while (1) {
			miss();
			if (c_type == LEX_COMMA) {
				miss();
				det_lex(",", LEX_NUM);
				rqst.const_num_list.push_back(lexem.get_int_value());
			} else {
				det_lex("NUM", LEX_RIGHT_PARENTHESIS);
				gl();
				return;	
			}
		}
	} else if (c_type == LEX_STR && is_str_list) {
		rqst.const_str_list.push_back(string(lexem.get_str_value()));
		while (1) {
			miss();
			if (c_type == LEX_COMMA) {
				miss();
				det_lex(",", LEX_STR);
				rqst.const_str_list.push_back(string(lexem.get_str_value()));
			} else {
				det_lex("STR", LEX_RIGHT_PARENTHESIS);
				gl();
				return;	
			}
		}
	} else {
		throw Err_Synt("expected list of typized const");
	}
}

void Synt_Scanner::check_types(const type_of_lex &op, const type_of_lex &a, const type_of_lex &b) {
	switch (op) {
	case LEX_EQUALS:
	case LEX_GREATER_THEN:
	case LEX_LESS_THEN:
	case LEX_GREATER_OR_EQUAL_THEN:
	case LEX_LESS_OR_EQUAL_THEN:
	case LEX_NOT_EQUAL:
		if ( ((a == LEX_NUM || a == LEX_ID_NUM) && (b == LEX_NUM || b == LEX_ID_NUM))
		  || ((a == LEX_STR || a == LEX_ID_STR) && (b == LEX_STR || b == LEX_ID_STR)) ) {
			sem_push(LEX_BOOL);
		} else {
			throw Err_Synt("incorrect types logic operation");
		}
		break;
	case LEX_PLUS:
	case LEX_DASH:
	case LEX_ASTERISK:
	case LEX_PERCENT:
	case LEX_SOLIDUS:
		if ((a == LEX_NUM || a == LEX_ID_NUM) && (b == LEX_NUM || b == LEX_ID_NUM)) {
			sem_push(LEX_NUM);
		} else {
			throw Err_Synt("incorrect types arifmetic operation");
		}
		break; 
	case LEX_OR:
	case LEX_AND:
		if ((a == LEX_BOOL) && (b == LEX_BOOL)) {
			sem_push(LEX_BOOL);
		} else {
			throw Err_Synt("incorrect types logic operation");		
		}
		break;
	default:
		throw Err_Synt("incorrect operation");
	}
	return;
}

void Synt_Scanner::State_Expression() {
	type_of_lex a, b;

	State_A();
	while (1) {
		if (c_type == LEX_OR) {
			miss();
			State_A();
			sem_pop();
			b = sem_st_type;
			sem_pop();
			a = sem_st_type;
			check_types(LEX_OR, a, b);
			lex_add(Lex(LEX_OR, 0, nullptr));
		} else {
			break;
		}
	}
	return;
}

void Synt_Scanner::State_A() {
	type_of_lex a, b;

	State_B();
	while (1) {
		if (c_type == LEX_AND) {
			miss();
			State_B();
			sem_pop();
			b = sem_st_type;
			sem_pop();
			a = sem_st_type;
			check_types(LEX_AND, a, b);
			lex_add(Lex(LEX_AND, 0, nullptr));
		} else {
			break;
		}
	}
	return;
}

void Synt_Scanner::State_B() {
	if (c_type == LEX_NOT) {
		gl();
		det_lex("NOT", LEX_SPACE);
		miss();
		State_B();
		sem_pop();
		if (sem_st_type != LEX_BOOL) {
			throw Err_Synt("incorrect type for NOT");
		}
		sem_push(LEX_BOOL);
		lex_add(Lex(LEX_NOT, 0, nullptr));
	} else {
		State_C();
		State_F();
	}
	return;
}

void Synt_Scanner::State_C() {
	type_of_lex op, a, b, ops;

	State_D();
	while (1) {
		ops = op = c_type;
		if ((op == LEX_PLUS || op == LEX_DASH)) {
			miss();
			State_D();
			sem_pop();
			b = sem_st_type;
			sem_pop();
			a = sem_st_type;
			check_types(op, a, b);
			lex_add(Lex(ops, 0, nullptr));
		} else {
			break;
		}
	}
	return;
}

void Synt_Scanner::State_D() {
	type_of_lex op, a, b, ops;

	State_E();
	while (1) {
		miss();
		ops = op = c_type;
		if (op == LEX_ASTERISK || op == LEX_PERCENT || op == LEX_SOLIDUS) {
			miss();
			State_E();
			sem_pop();
			b = sem_st_type;
			sem_pop();
			a = sem_st_type;
			check_types(op, a, b);
			lex_add(Lex(ops, 0, nullptr));
		} else {
			break;
		}
	}
	return;
}

void Synt_Scanner::State_E() {
	FieldType type;
	auto i = rqst.list_tables.begin();

	if (c_type == LEX_NUM || c_type == LEX_STR) {
		sem_push(c_type);
		lex_add(lexem);
	} else if (c_type == LEX_ID) {
		for ( ; i != rqst.list_tables.end(); i++) {
			try {
				i->Get_Field_Type(i->get_handle(), lexem.get_str_value(), &type);
				break;
			}
			catch(Err_Table e) {
				if (strcmp(e.getstr(), ErrorText[FieldNotFound]) != 0) {
					throw;
				}
			}
		}
		if (i == rqst.list_tables.end()) {
			throw Err_Table("Field Not Found");
		}
		if (type == Long) {
			sem_push(LEX_ID_NUM);
			lex_add(lexem = Lex(LEX_ID_NUM, lexem.get_int_value(), lexem.get_str_value()));
		} else {
			sem_push(LEX_ID_STR);
			lex_add(lexem = Lex(LEX_ID_STR, 0, lexem.get_str_value()));
		}
	} else if (c_type == LEX_LEFT_PARENTHESIS) {
		miss();
		State_Expression();
		det_lex("expression", LEX_RIGHT_PARENTHESIS);
	} else {
		throw Err_Synt("incorrect expression");
	}
	return;
}

void Synt_Scanner::State_F() {
	type_of_lex op, a, b, ops;

	ops = op = c_type;
	if (op == LEX_EQUALS || op == LEX_GREATER_THEN || op == LEX_LESS_THEN ||
	  op == LEX_GREATER_OR_EQUAL_THEN || op == LEX_LESS_OR_EQUAL_THEN ||
	  op == LEX_NOT_EQUAL) {
		miss();
		State_C();
		sem_pop();
		b = sem_st_type;
		sem_pop();
		a = sem_st_type;
		check_types(op, a, b);
		lex_add(Lex(ops, 0, nullptr));
	} else {
		;
	}
	return;
}
