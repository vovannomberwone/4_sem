#pragma once

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <cstring>
#include "SQL_Executer.h"
#include "Err_Lex.h"
#include "Err_Table.h"
#include "Err_Synt.h"
#include "Err_Executer.h"

class SQL_Server {
private:
	static const int port_min = 1024;
	static const int port_max = 65535;
	static const int msg_size = 256;
	static const int hostname_size = 64;
	const long error = -1;

	FILE *fp;
	char c;
	int sd, sd_new, fromlen, port, i, len, opt = 1;
	long sz;
	bool is_end;
	char hostname[hostname_size];
	struct hostent *hp;
	struct sockaddr_in sin, fromsin;
public:
	SQL_Server();
	~SQL_Server() = default;
	void close_sds();
	void send_file(const char *f_name);
};