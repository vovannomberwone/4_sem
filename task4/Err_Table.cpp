#include "Err_Table.h"
#include <iostream>

using namespace std;

Err_Table::Err_Table(const Err_Table &e): Error(e) {
	;
}

Err_Table::Err_Table(const char* s) {
	putstr(s);
}

Err_Table& Err_Table::operator = (const Err_Table &e) {
	putstr(e.getstr());
	return *this;
}

void Err_Table::cout_str() {
	cout << "Error when make operation with table:\n" << getstr() << endl;
	return;
}
