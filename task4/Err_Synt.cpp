#include "Err_Synt.h"
#include <iostream>

using namespace std;

Err_Synt::Err_Synt(const Err_Synt &e): Error(e) {
	;
}

Err_Synt::Err_Synt(const char* s) {
	putstr(s);
}

Err_Synt& Err_Synt::operator = (const Err_Synt &e) {
	putstr(e.getstr());
	return *this;
}

void Err_Synt::cout_str() {
	cout << "Error when make syntax analysis:\n" << getstr() << endl;
	return;
}
