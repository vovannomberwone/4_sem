#include <stack>
#include <cstring>
#include <cstdio>
#include <string>
#include "SQL_Executer.h"
#include "Err_Executer.h"
#include "Synt_Scanner.h"
#include "SQL_Request.h"
#include "TableOP.h"

using namespace std;

SQL_Executer::SQL_Executer(FILE *fp): s_scanner(fp) {
	;
}

SQL_Executer::~SQL_Executer() {
	;
}

const char* SQL_Executer::get_table_name() {
	return table_name;
}

bool SQL_Executer::Execute() {
	s_scanner.scan_request();
	SQL_Request rqst = s_scanner.get_request();

	try {
		switch(rqst.type) {
		case EMPTY_REQUEST:
			break;
		case SELECT_REQUEST:
			Ex_Select();
			break;
		case INSERT_REQUEST:
			Ex_Insert();
			break;
		case UPDATE_REQUEST:
			Ex_Update();
			break;
		case DELETE_REQUEST:
			Ex_Delete();
			break;
		case CREATE_REQUEST:
			Ex_Create();
			break;
		case DROP_REQUEST:
			Ex_Drop();
			break;
		case FIN_REQUEST:
			return TRUE;
		default:
			throw Err_Executer("this message cant be seen");
			break;
		}
	}
	catch (...) {
		for (auto i = rqst.list_tables.begin(); i != rqst.list_tables.end(); i++) {
			i->is_open = FALSE;
		}
		throw;
	}
	for (auto i = rqst.list_tables.begin(); i != rqst.list_tables.end(); i++) {
		i->is_open = FALSE;
	}
	return FALSE;
}

void SQL_Executer::Ex_Select() {
	SQL_Request rqst = s_scanner.get_request();
	vector<int> recs;
	long ct_check = 1;
	char tmp_name[L_tmpnam];
	long num, j;
	char *text, *fname;

	try {
		if (rqst.same_tables_or_fields()) {
			throw Err_Executer("The same tables or same fields in tables or same fields on request are identified");
		}
		
		for (auto i = rqst.list_tables.begin(); i != rqst.list_tables.end(); i++) {
			ct_check *= i->ct_rec;
			recs.push_back(i->ct_rec);
			i->Move_First(i->get_handle());
		}

		if (rqst.all_fields) {
			for (auto i = rqst.list_tables.begin(); i != rqst.list_tables.end(); i++) {
				for (j = 0; j < i->ct_field; j++) {
					i->Get_Field_Name(i->get_handle(), j, &fname);
					rqst.select_list.push_back(string(fname));
				}
			}
		}

		tmpnam(tmp_name);
		TableStruct ts;
		FieldDef *fields = new FieldDef[rqst.select_list.size()];
		long cur_field = 0;
		FieldType ftype;
		unsigned int flen;
		for (auto i = rqst.list_tables.begin(); i != rqst.list_tables.end(); i++) {
			for (j = 0; j < i->ct_field; j++) {
				i->Get_Field_Name(i->get_handle(), j, &fname);
				for (auto k = rqst.select_list.begin(); k != rqst.select_list.end(); k++) {
					if (strcmp(k->c_str(), fname) == 0) {		
						strcpy(fields[cur_field].name, fname);
						i->Get_Field_Type(i->get_handle(), fname, &ftype);
						fields[cur_field].type = ftype;
						i->Get_Field_Len(i->get_handle(), fname, &flen);
						fields[cur_field].len = flen;
						cur_field++;
					}
				}
			}
		}
		ts.fieldsDef = fields;
		ts.numOfFields = rqst.select_list.size(); 
		TableOP table;
		table.Create_Table(tmp_name, &ts);
		delete []fields;
		table.Get_Info(tmp_name, FALSE);
		
		while (ct_check-- > 0) {
			if (Logic_Expression(rqst)) {
				table.Create_New(table.get_handle());
				for (auto i = rqst.list_tables.begin(); i != rqst.list_tables.end(); i++) {
					for (j = 0; j < i->ct_field; j++) {
						i->Get_Field_Name(i->get_handle(), j, &fname);
						for (auto k = rqst.select_list.begin(); k != rqst.select_list.end(); k++) {
							if (strcmp(k->c_str(), fname) == 0) {
								i->Get_Field_Type(i->get_handle(), fname, &ftype);
								if (ftype == Long) {
									i->Get_Long(i->get_handle(), fname, &num);
								 	table.Put_Long_New(table.get_handle(), fname, num);
								} else if (ftype == Text) {
									i->Get_Text(i->get_handle(), fname, &text);
									table.Put_Text_New(table.get_handle(), fname, text);
								} else {
									throw Err_Executer("Internal error: Incorrect type of field");
								}
							}
						}
					}
				}
				table.Insertz_New(table.get_handle());
				table.Move_Last(table.get_handle());
				table.Get_Text(table.get_handle(), "First_name", &text);
			}
			rqst.list_tables.rbegin()->Move_Next(rqst.list_tables.rbegin()->get_handle());
			for (auto i = rqst.list_tables.rbegin(); i != rqst.list_tables.rend(); ) {
				if (i->After_Last(i->get_handle())) {
					i->Move_First(i->get_handle());
					i++;
					if (i != rqst.list_tables.rend()) {
						i->Move_Next(i->get_handle());
					}
				} else {
					break;
				}
			}
		}
		table.Close_Table(table.get_handle());
		table.is_open = FALSE;

		strcpy(table_name, tmp_name);
	}
	catch (...) {
		rqst.cls_Tables();
		throw;
	}

	rqst.cls_Tables();
	return;
}

void SQL_Executer::Ex_Insert() {
	char *fname;
	SQL_Request rqst = s_scanner.get_request();
	TableOP t = *(rqst.list_tables.begin());
	
	try {
 		t.Create_New(t.get_handle());
 		if (rqst.insert_list.size() != t.ct_field) {
 			throw Err_Executer("count of fields in table does not match with count of fields in request");
 		}
 		int j = 0;
 		for (auto i = rqst.insert_list.begin(); i != rqst.insert_list.end(); i++) {
 			t.Get_Field_Name(t.get_handle(), j, &fname);
 			if (i->get_type() == LEX_STR) {
 				t.Put_Text_New(t.get_handle(), fname, i->get_str_value());
 			} else if (i->get_type() == LEX_NUM) {
 				t.Put_Long_New(t.get_handle(), fname, i->get_int_value());
 			} else {
 				throw Err_Executer("this message cant be seen");
 			}
 			j++;
 		}
 		t.Insertz_New(t.get_handle());
 	}
 	catch (...) {
		rqst.list_tables.begin()->is_open = t.is_open = FALSE;
		throw;	 		
 	}

	rqst.list_tables.begin()->is_open = t.is_open = FALSE; // т.к при удалении rqst и t закроется таблица
	return;
}

void SQL_Executer::Ex_Update() {
	SQL_Request rqst = s_scanner.get_request();
	TableOP t = *(rqst.list_tables.begin());
	FieldType ptype;
	
	try {
		t.Get_Field_Type(t.get_handle(), rqst.name_update, &ptype);
		t.Move_First(t.get_handle());
		for (unsigned int i = 0; i < t.ct_rec; i++) {
			if (Logic_Expression(rqst)) {
				t.Start_Edit(t.get_handle());
				if (ptype == Long) {
					t.Put_Long(t.get_handle(), rqst.name_update, (Expression(rqst, rqst.expr_update_list, FALSE)).get_int_value());
				} else if (ptype == Text) {
					t.Put_Text(t.get_handle(), rqst.name_update, (Expression(rqst, rqst.expr_update_list, FALSE)).get_str_value());
				} else {
					throw Err_Executer("this message cant be seen");
				}
				t.Finish_Edit(t.get_handle());
			}
			t.Move_Next(t.get_handle());
		}
	}
	catch (...) {
		rqst.list_tables.begin()->is_open = t.is_open = FALSE;
		throw;	 		
	}

	rqst.list_tables.begin()->is_open = t.is_open = FALSE;
	return;
}

void SQL_Executer::Ex_Delete() {
	SQL_Request rqst = s_scanner.get_request();
	TableOP t = *(rqst.list_tables.begin());

	try {
		t.Move_First(t.get_handle());
		for (unsigned int i = 0; i < t.ct_rec; i++) {
			if (Logic_Expression(rqst)) {
				t.Delete_Rec(t.get_handle());
			}
			t.Move_Next(t.get_handle());
		}
	}
	catch (...) {
		rqst.list_tables.begin()->is_open = t.is_open = FALSE;
		throw;	 		
	}

	rqst.list_tables.begin()->is_open = t.is_open = FALSE;
	return;
}

void SQL_Executer::Ex_Create() {
	SQL_Request rqst = s_scanner.get_request();
	TableOP t = *(rqst.list_tables.begin());
	
	TableStruct ts;
	FieldDef *fields = new FieldDef[rqst.create_list.size()];
	int j = 0;
	for (auto i = rqst.create_list.begin(); i != rqst.create_list.end(); i++) {
		strcpy(fields[j].name, i->name);
		fields[j].type = i->type;
		fields[j].len = i->len;
		j++;
	}
	ts.fieldsDef = fields;
	ts.numOfFields = rqst.create_list.size(); 
	t.Create_Table(t.get_table_name(), &ts);

	delete []fields;
	return;
}

void SQL_Executer::Ex_Drop() {
	SQL_Request rqst = s_scanner.get_request();
	TableOP t = *(rqst.list_tables.begin());

	try {	
		t.Close_Table(t.get_handle());
	}
	catch (...) {
		t.is_open = FALSE;
		throw;	 		
	}

	s_scanner.rqst.cls_Tables();
	rqst.list_tables.begin()->is_open = t.is_open = false;
	t.Delete_Table(t.get_table_name());
	return;	
}

void SQL_Executer::get_operands(stack<Lex> &stack, Lex &op1, Lex &op2, const type_operation &t) const {
	op2 = stack.top();
	stack.pop();
	op1 = stack.top();
	stack.pop();
	switch (t) {
	case LOGIC:
		if (op1.get_type() == LEX_BOOL && op2.get_type() == LEX_BOOL) {
			;
		} else {
			throw Err_Executer("incorrect operands of ogic operation");
		}
		break;
	case COMPARSION:
		if (op1.get_type() == LEX_STR && op2.get_type() == LEX_STR) {
			;
		} else if (op1.get_type() == LEX_NUM && op2.get_type() == LEX_NUM) {
			;
		} else {
			throw Err_Executer("incorrect operands of operation of COMPARSION");
		}
		break;
	case ARIFMETIC:
		if (op1.get_type() == LEX_NUM && op2.get_type() == LEX_NUM) {
			;
		} else {
			throw Err_Executer("incorrect operands of ARIFMETIC operation");
		}
		break;
	default:
		throw Err_Executer("this message are invisible");
	}
	return;
}
	
bool SQL_Executer::Logic_Expression(const SQL_Request &rqst) {
	bool ok = FALSE;
	const Lex l = Expression(rqst, rqst.expr_list, TRUE);

	if (rqst.where_all) {
		return TRUE;
	} else if (l.get_type() == LEX_BOOL) {
		return l.get_int_value();
	} else if (rqst.is_like) { 
		return ((!(like_strcmp(l.get_str_value(), rqst.str_like))) ^ rqst.is_not);
	} else if (rqst.is_in) {
		if (l.get_type() == LEX_STR) {
			for (auto i = rqst.const_str_list.begin(); i != rqst.const_str_list.end(); i++) {
				if (strcmp(i->c_str(), l.get_str_value()) == 0) {
					ok = TRUE;
					break;
				}
			}
			return ok ^ rqst.is_not;
		} else if (l.get_type() == LEX_NUM) {
			for (auto i = rqst.const_num_list.begin(); i != rqst.const_num_list.end(); i++) {
				if (*i == l.get_int_value()) {
					ok = TRUE;
					break;
				}
			}
			return ok ^ rqst.is_not;
		} else {
			throw Err_Executer("incorrect Expression");	
		}
	} else {
		throw Err_Executer("incorrect Expression");
	}
	return FALSE;
}

void SQL_Executer::Expression_ID_NUM(const SQL_Request &rqst, const Lex &l, const list<Lex> &expr_list, stack<Lex> &expr_stack) const {
	long num;

	auto j = rqst.list_tables.begin();
	for ( ; j != rqst.list_tables.end(); j++) {
		try {
			j->Get_Long(j->get_handle(), l.get_str_value(), &num);
			break;
		}
		catch(Err_Table e) {
			if (strcmp(e.getstr(), ErrorText[FieldNotFound]) != 0) {
				throw;
			}
		}
	}
	if (j == rqst.list_tables.end()) {
		throw Err_Table("Field Not Found");
	}
	expr_stack.push(Lex(LEX_NUM, num, nullptr));
	return;
}

void SQL_Executer::Expression_ID_STR(const SQL_Request &rqst, const Lex &l, const list<Lex> &expr_list, stack<Lex> &expr_stack) const {
	char *text;

	auto j = rqst.list_tables.begin();
	for ( ; j != rqst.list_tables.end(); j++) {
		try {
			j->Get_Text(j->get_handle(), l.get_str_value(), &text);
			break;
		}
		catch(Err_Table e) {
			if (strcmp(e.getstr(), ErrorText[FieldNotFound]) != 0) {
				throw;
			}
		}
	}
	if (j == rqst.list_tables.end()) {
		throw Err_Table("Field Not Found");
	}
	expr_stack.push(Lex(LEX_STR, 0, text));
	return;
}

const Lex SQL_Executer::Expression(const SQL_Request &rqst, const list<Lex> &expr_list, bool where_expr) const {
	stack<Lex> expr_stack;
	type_of_lex lex_type;
	Lex op1, op2;
	
	if (rqst.where_all && where_expr) {
		return Lex(LEX_NULL, 0, nullptr);
	}
	for (auto i = expr_list.begin(); i != expr_list.end(); i++) {
		lex_type = i->get_type();
		switch (lex_type) {
		case LEX_NUM:
		case LEX_STR:
			expr_stack.push(*i);
			break;
		case LEX_ID_NUM:
			Expression_ID_NUM(rqst, *i, expr_list, expr_stack);
			break;
		case LEX_ID_STR: 
			Expression_ID_STR(rqst, *i, expr_list, expr_stack);
			break;
		case LEX_EQUALS:
			get_operands(expr_stack, op1, op2, COMPARSION);
			if (op1.get_type() == LEX_STR && op2.get_type() == LEX_STR) {
				expr_stack.push(Lex(LEX_BOOL, strcmp(op1.get_str_value(), op2.get_str_value()) == 0, nullptr));
			} else if (op1.get_type() == LEX_NUM && op2.get_type() == LEX_NUM) {
				expr_stack.push(Lex(LEX_BOOL, op1.get_int_value() == op2.get_int_value(), nullptr));
			}
			break;
		case LEX_GREATER_THEN:
			get_operands(expr_stack, op1, op2, COMPARSION);
			if (op1.get_type() == LEX_STR && op2.get_type() == LEX_STR) {
				expr_stack.push(Lex(LEX_BOOL, strcmp(op1.get_str_value(), op2.get_str_value()) > 0, nullptr));
			} else if (op1.get_type() == LEX_NUM && op2.get_type() == LEX_NUM) {
				expr_stack.push(Lex(LEX_BOOL, op1.get_int_value() > op2.get_int_value(), nullptr));
			}
			break;
		case LEX_LESS_THEN:
			get_operands(expr_stack, op1, op2, COMPARSION);
			if (op1.get_type() == LEX_STR && op2.get_type() == LEX_STR) {
				expr_stack.push(Lex(LEX_BOOL, strcmp(op1.get_str_value(), op2.get_str_value()) < 0, nullptr));
			} else if (op1.get_type() == LEX_NUM && op2.get_type() == LEX_NUM) {
				expr_stack.push(Lex(LEX_BOOL, op1.get_int_value() < op2.get_int_value(), nullptr));
			}
			break;
		case LEX_GREATER_OR_EQUAL_THEN:
			get_operands(expr_stack, op1, op2, COMPARSION);
			if (op1.get_type() == LEX_STR && op2.get_type() == LEX_STR) {
				expr_stack.push(Lex(LEX_BOOL, strcmp(op1.get_str_value(), op2.get_str_value()) >= 0, nullptr));
			} else if (op1.get_type() == LEX_NUM && op2.get_type() == LEX_NUM) {
				expr_stack.push(Lex(LEX_BOOL, op1.get_int_value() >= op2.get_int_value(), nullptr));
			}
			break;
		case LEX_LESS_OR_EQUAL_THEN:
			get_operands(expr_stack, op1, op2, COMPARSION);
			if (op1.get_type() == LEX_STR && op2.get_type() == LEX_STR) {
				expr_stack.push(Lex(LEX_BOOL, strcmp(op1.get_str_value(), op2.get_str_value()) <= 0, nullptr));
			} else if (op1.get_type() == LEX_NUM && op2.get_type() == LEX_NUM) {
				expr_stack.push(Lex(LEX_BOOL, op1.get_int_value() <= op2.get_int_value(), nullptr));
			}
			break;
		case LEX_NOT_EQUAL:
			get_operands(expr_stack, op1, op2, COMPARSION);
			if (op1.get_type() == LEX_STR && op2.get_type() == LEX_STR) {
				expr_stack.push(Lex(LEX_BOOL, strcmp(op1.get_str_value(), op2.get_str_value()) != 0, nullptr));
			} else if (op1.get_type() == LEX_NUM && op2.get_type() == LEX_NUM) {
				expr_stack.push(Lex(LEX_BOOL, op1.get_int_value() != op2.get_int_value(), nullptr));
			}
			break;
		case LEX_PLUS:
			get_operands(expr_stack, op1, op2, ARIFMETIC);
			expr_stack.push(Lex(LEX_NUM, op1.get_int_value() + op2.get_int_value(), nullptr));
			break;
		case LEX_DASH:
			get_operands(expr_stack, op1, op2, ARIFMETIC);
			expr_stack.push(Lex(LEX_NUM, op1.get_int_value() - op2.get_int_value(), nullptr));
			break;
		case LEX_ASTERISK:
			get_operands(expr_stack, op1, op2, ARIFMETIC);
			expr_stack.push(Lex(LEX_NUM, op1.get_int_value() * op2.get_int_value(), nullptr));
			break;
		case LEX_PERCENT:
			get_operands(expr_stack, op1, op2, ARIFMETIC);
			expr_stack.push(Lex(LEX_NUM, op1.get_int_value() % op2.get_int_value(), nullptr));
			break;
		case LEX_SOLIDUS:
			get_operands(expr_stack, op1, op2, ARIFMETIC);
			expr_stack.push(Lex(LEX_NUM, op1.get_int_value() / op2.get_int_value(), nullptr));
			break; 
		case LEX_OR:
			get_operands(expr_stack, op1, op2, LOGIC);
			expr_stack.push(Lex(LEX_BOOL, op1.get_int_value() || op2.get_int_value(), nullptr));
			break;
		case LEX_AND:
			get_operands(expr_stack, op1, op2, LOGIC);
			expr_stack.push(Lex(LEX_BOOL, op1.get_int_value() && op2.get_int_value(), nullptr));
			break;
		case LEX_NOT:
			op1 = expr_stack.top();
			expr_stack.pop();
			if (op1.get_type() != LEX_BOOL) {
				throw Err_Executer("incorrect type for NOT");
			}
			expr_stack.push(Lex(LEX_BOOL, !(op1.get_int_value()), nullptr));
			break;	
		default:
			cout << *i;
			throw Err_Executer("this message not visible");
		}
	}
	return expr_stack.top();
}

int SQL_Executer::like_strcmp(const char *s1, const char *s2) {	// s2 - str_like
	char first;
	bool not_in;
	const char *p1 = s1;
	const char *p2 = s2;
 	while (1) {
 		while (*p1 == *p2) {
 			if (*p1 == '\0') {
 				while (*p2 == '%') {
 					p2++;
 				}
 				if (*p2 != '\0') {
 					return 1;
 				} else {
 					return 0;
 				}
 			}
 			p1++;
 			p2++;
 		}
 		if (*p2 == '_') {
 			p1++;
 			p2++;
 		} else if (*p2 == '[') {
 			p2++;
 			if (*p2 == '^') {
 				not_in = TRUE;
 				p2++;
 			}
 			if (*p2 == '\0') {
 				return 1;
	 		}
	 		if (*p2 == *p1 && !(not_in)) {
	 			p2++;
	 			if (*p2 == '\0') {
	 				return 1;
	 			}
	 			while (*p2 != ']') {
	 				p2++;
	 				if (*p2 == '\0') {
	 					return 1;
	 				}
	 			}
	 			p2++;
	 			p1++;
	 			continue;
	 		} else if (*p2 == *p1 && not_in) {
	 			return 1;
	 		} else {
	 			first = *p2;
	 			p2++;
	 		}
	 		if (*p2 == '\0') {
	 			return 1;
	 		}
	 		if (*p2 == '-') {
	 			p2++;
	 			if (*p2 == '\0') {
	 				return 1;
	 			}
	 			if (first <= *p1 && *p1 <= *p2 && (!not_in)) {
	 				p1++;
	 				p2++;
	 				if (*p2 != ']') {
	 					return 1;
	 				} else {
	 					p2++;
	 				}
	 			} else {
	 				return 1;
	 			}
	 		} else {
	 			while (1) {
	 				if (*p2 == *p1) {
	 					while (1) {
	 						p2++;
		 					if (*p2 == '\0') {
		 						return 1;
		 					}
		 					if (*p2 == ']') {
		 						p2++;
		 						break;
		 					}	
	 					}
	 					p1++;
	 					break;
	 				} else {
	 					p2++;
	 					if (*p2 == '\0') {
	 						return 1;
	 					}
	 					if (*p2 == ']') {
	 						return 1;
	 					}
	 				}
	 			}
	 		}
	 	} else if (*p2 == '%') {
	 		p2++;
	 		while (*p1 != '\0') {
		 		if (like_strcmp(p1, p2) == 0) {
		 			return 0;
		 		} else {
		 			p1++;
		 		}
		 	}
		} else {
			return 1;
	 	}
	}
}