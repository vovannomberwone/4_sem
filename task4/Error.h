#pragma once

class Error {
private:
	static const int mlen_str = 150;
	char str[mlen_str + 1];
	virtual void cout_str() = 0;
public:
	Error() = default;
	~Error() = default;
	Error(const Error &e);
	Error& operator = (const Error &e);
	int putstr(const char *s);
	const char* getstr() const;
};