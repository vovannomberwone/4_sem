#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <cstring>
#include "SQL_Executer.h"
#include "Error.h"
#include "Err_Lex.h"
#include "Err_Table.h"
#include "Err_Synt.h"
#include "Err_Executer.h"
#include "SQL_Server.h"

void SQL_Server::close_sds() {
	fclose(fp);
	close(sd_new);
	close(sd);
	return;
}

void SQL_Server::send_file(const char *f_name) {
	int n, fd;
	char buf[msg_size];
	long f_size = 0;

	fd = open(f_name, O_RDONLY, 0666);
	while ((n = read(fd, buf, sizeof(buf))) > 0) {
		f_size += n; 
	}
	lseek(fd, 0 , SEEK_SET);
	write(sd_new, &f_size, sizeof(f_size));
	while ((n = read(fd, buf, sizeof(buf))) > 0) {
		write(sd_new, buf, n);
	}
	close(fd);
	return;
}

SQL_Server::SQL_Server() { 
	if ((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0) { 
		perror("server: socket"); 
		throw "server: socket";
	}
	
	printf("Enter port\n");
	port = 0;
	while (1) {
		while (1) {
			if (read(0, &c, 1) <= 0) {
				throw "cant read port";
			}
			if (c <= '9' && c >= '0') {
				port = port * 10 + c;
				if (port < 0) {
					throw "too long port";
				}
			} else if (c == '\n') {
				if (port > 0) {
					break;
				} else {
					throw "empty port";
				}
			} else {
				throw "expected Enter after port";
			}
		}
		if (!(port >= port_min && port <= port_max)) {
			printf("Enter port from %d to %d\n", port_min, port_max);
		} else {
			break;
		}
	}

	sin.sin_port = htons(port);
	sin.sin_family = AF_INET;
	gethostname(hostname, sizeof(hostname));
	if ((hp = gethostbyname(hostname)) == NULL) {
		fprintf(stderr, "%s: unknown host\n", hostname);
		close(sd);
		throw "server: unknown host";
	}
	memcpy(&sin.sin_addr, hp->h_addr, hp->h_length);
	setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
	if (bind(sd, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
		perror("server: bind");
		close(sd);
		throw "server: bind";
	}
	if (listen(sd, 5) < 0) {
		perror("server: listen");
		close(sd);
		throw "server: listen";
	}
	fromlen = sizeof(fromsin);
	if ((sd_new = accept(sd, (struct sockaddr *)&fromsin, (socklen_t *)&fromlen)) < 0) {
		perror("server: accept");
		close(sd);
		throw "server: accept";
	}
	fp = fdopen(sd_new, "r");
	SQL_Executer SQL_Ex(fp);
	while (1) {
		try {
			is_end = SQL_Ex.Execute();
			if (is_end) {
				close_sds();
				throw "cant read or end session";
			}
			if (SQL_Ex.s_scanner.rqst.type == SELECT_REQUEST) {
				send_file(SQL_Ex.get_table_name());
			}
		}
		catch (Err_Lex err) {
			err.cout_str();
			write(sd_new, &error, sizeof(error));
			sz = strlen(err.getstr()) + 1;
			write(sd_new, &sz, sizeof(sz));
			write(sd_new, err.getstr(), sz);
			close_sds();
			throw "lexical error";
		}
		catch (Err_Table err) {
			err.cout_str();
			write(sd_new, &error, sizeof(error));
			sz = strlen(err.getstr()) + 1;
			write(sd_new, &sz, sizeof(sz));
			write(sd_new, err.getstr(), sz);
			close_sds();
			throw "table error";
		}
		catch (Err_Synt err) {
			err.cout_str();
			write(sd_new, &error, sizeof(error));
			sz = strlen(err.getstr()) + 1;
			write(sd_new, &sz, sizeof(sz));
			write(sd_new, err.getstr(), sz);
			close_sds();
			throw "syntax error";
		}
		catch (Err_Executer err) {
			err.cout_str();
			write(sd_new, &error, sizeof(error));
			sz = strlen(err.getstr()) + 1;
			write(sd_new, &sz, sizeof(sz));
			write(sd_new, err.getstr(), sz);
			close_sds();
			throw "execute error";
		}
	}
	close_sds();
}