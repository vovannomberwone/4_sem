#include <iostream>
#include <vector>
#include <cstring>
#include "Lex.h"
#include "Lex_Scanner.h"
#include "Err_Lex.h"

using namespace std;

const char* Lex_Scanner::TW[] = { 
		"NULLSTR", 
	//	  0
		"FROM", "SELECT", "UPDATE", "INSERT", "INTO", "CREATE", "TABLE",
	//    1        2         3         4        5        6         7
		"DELETE", "DROP", "SET", "TEXT", "LONG", "WHERE", "NOT", "LIKE", "IN",
	//	  8         9      10      11      12      13       14     15     16
		"ALL", "OR", "AND",
	//	  17     18    19
		"+", "-", "*", "%", "/", ",", " ", "'", "(", ")",
	//	 20   21   22   23   24   25   26   27   28   29
		"=", ">", "<", ">=", "<=", "!=", "[", "]", "\n", "ID",
	//	 30   31   32   33    34    35    36   37   38    39
		"NUM", "STR", "FIN", "BOOL"
	//	 40     41     42     43    
};

const char Lex_Scanner::alphid[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
const char Lex_Scanner::letters[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
const char Lex_Scanner::dig[] = "0123456789";
const char Lex_Scanner::common_del[] = "+-*%/, ()[]=";
const char Lex_Scanner::delims[] = "+-*%/, '()=><![]_";

void Lex_Scanner::gc() {
	c = fgetc(fp);
	return;
}

void Lex_Scanner::clear_buf() {
	buf_top = 0;
	return;
}

void Lex_Scanner::cut() {
	buf[buf_top] = '\0';
	return;
}

void Lex_Scanner::add() {
	if (buf_top == mlen_id) {
		cut();
		strcpy(str, "max leng of identificator or string was excedeed: ");
		strcat(str, buf);
		throw Err_Lex(str);
	} else {
		buf[buf_top] = c;
		buf_top++;
	}
	return;
}

int Lex_Scanner::instr(const char c, const char *str) {
	int pos = 0;
	while (c != str[pos] && str[pos]) {
		pos++;
	}
	if ((unsigned)pos == strlen(str)) {
		return -1;
	} else {
		return pos;
	}
}

Lex_Scanner::Lex_Scanner(FILE* file_pointer) {
	buf_top = 0;
	fp = file_pointer;
	gc();
}

Lex_Scanner::~Lex_Scanner() {
	fclose(fp);
}

void Lex_Scanner::State_Ident_or_Slave(Lex &lexem) {
	type_of_lex t;
	while (instr(c, alphid) >= 0) {
		add();
		gc();
	}
	cut();
	for (t = LEX_FROM; t < LEX_PLUS; t = type_of_lex(t + 1)) {
		if (strcmp(buf, TW[t]) == 0) {
			lexem = Lex(t, 0, nullptr);
			return;
		}
	}
	lexem = Lex(LEX_ID, 0, buf);
	return;
}

void Lex_Scanner::State_Numb(Lex &lexem) {
	long num = 0, prev = 0;
	while (instr(c, dig) >= 0 && num >= prev) {
		prev = num;
		num = num * 10 + (c - '0'); 
		gc();
	}
	if (num >= prev) {
		if (instr(c, delims) >= 0 || c == EOF || c == '\n') {
			lexem = Lex(LEX_NUM, num, nullptr);
		} else {
			strcpy(str, "expeected delim after long but ");
			str[strlen(str)] = c;
			strcat(str, " detected");
			throw Err_Lex(str);
		}
	} else {
		strcpy(str, "max value of long was excedeed: ");
		strcat(str, buf);
		throw Err_Lex(str);
	}
	return;
}

void Lex_Scanner::State_Com_Del(Lex &lexem) {
	switch (c) {
	case '+':
		lexem = Lex(LEX_PLUS, 0, nullptr);
		break;
	case '-':
		lexem = Lex(LEX_DASH, 0, nullptr);
		break;
	case '*':
		lexem = Lex(LEX_ASTERISK, 0, nullptr);
		break;
	case '%':
		lexem = Lex(LEX_PERCENT, 0, nullptr);
		break;
	case '/':
		lexem = Lex(LEX_SOLIDUS, 0, nullptr);
		break;
	case ',':
		lexem = Lex(LEX_COMMA, 0, nullptr);
		break;
	case '(':
		lexem = Lex(LEX_LEFT_PARENTHESIS, 0, nullptr);
		break;
	case ')':
		lexem = Lex(LEX_RIGHT_PARENTHESIS, 0, nullptr);
		break;
	case '[':
		lexem = Lex(LEX_LEFT_SQUARE_BRACKET, 0, nullptr);
		break;
	case ']':
		lexem = Lex(LEX_RIGHT_SQUARE_BRACKET, 0, nullptr);
		break;
	case '=':
		lexem = Lex(LEX_EQUALS, 0, nullptr);
		break;
	case ' ':
		lexem = Lex(LEX_SPACE, 0, nullptr);
		break;
	}
	gc();
	return;
}

void Lex_Scanner::State_GrLess(Lex &lexem) {
	int s = c;
	gc();
	if (c == '=') {
		if (s == '<') {
			lexem = Lex(LEX_LESS_OR_EQUAL_THEN, 0, nullptr);
		} else {
			lexem = Lex(LEX_GREATER_OR_EQUAL_THEN, 0, nullptr);
		}
		gc();
	} else {
		if (s == '<') {
			lexem = Lex(LEX_LESS_THEN, 0, nullptr);
		} else {
			lexem = Lex(LEX_GREATER_THEN, 0, nullptr);
		} 
	}
	return;
}

void Lex_Scanner::State_Not(Lex &lexem) {
	gc();
	if (c != '=') {
		strcpy(str, "expeected = after ! but ");
		str[strlen(str)] = c;
		strcat(str, " detected");
		throw Err_Lex(str);
	} else {
		lexem = Lex(LEX_NOT_EQUAL, 0, nullptr);
	}
	return;
}

void Lex_Scanner::State_Str(Lex &lexem) {
	gc();
	if (c == '\'') {
		strcpy(str, "expeected not empty str detected");
		throw Err_Lex(str);
	}
	while (c != '\'' && c != EOF) {
		add();
		gc();
	}
	if (c == EOF) {
		strcpy(str, "expeected ' but EOF detected");
		throw Err_Lex(str);
	}
	cut();
	lexem = Lex(LEX_STR, 0, buf);
	gc();
	return;
}

bool Lex_Scanner::getlex(Lex &lexem) {
	bool is_end = false; 
	clear_buf();
	if (instr(c, letters) >= 0) {
		State_Ident_or_Slave(lexem);
	} else if (instr(c, dig) >= 0) {
		State_Numb(lexem);
	} else if (instr(c, common_del) >= 0) {
		State_Com_Del(lexem);
	} else if (c == '<' || c == '>') {
		State_GrLess(lexem);
	} else if (c == '!') {
		State_Not(lexem);
	} else if (c == '\'') {
		State_Str(lexem);
	} else if (c == '\n' || c == EOF) {
		if (c == '\n') {
			lexem = Lex(LEX_NEL, 0, nullptr);
			gc(); // ---------------------------- if > 1 request
		}	
		if (c == EOF) {
			lexem = Lex(LEX_FIN, 0, nullptr);
		}
		is_end = true;
	} else {
		strcpy(str, "unknown symbol: ");
		str[strlen(str) + 1] = '\0';
		str[strlen(str)] = c;
		throw Err_Lex(str);
	}
	return is_end;
}

void Lex_Scanner::Scan() {
	unsigned int i = 0;
	vector<Lex> program;

	while (!(getlex(lexem))) {
		if (lexem.get_type() != LEX_NULL) {
			program.push_back(lexem);
		}
	}
	for (i = 0; i < program.size(); i++) {
		cout << program[i] << " "; 
	}
	return;
}