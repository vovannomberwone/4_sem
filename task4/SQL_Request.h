#pragma once

#include "TableOP.h"
#include <stack>
#include <list>
#include "Lex.h"
#include "string"

enum request_type {
	EMPTY_REQUEST, SELECT_REQUEST, INSERT_REQUEST, UPDATE_REQUEST,
	DELETE_REQUEST, CREATE_REQUEST, DROP_REQUEST, FIN_REQUEST
};

class SQL_Request {
private:
	friend class Synt_Scanner;  // access to stacks
	friend class SQL_Executer;
	friend class SQL_Server;

	static const int mlen_id = 80;

	request_type type;
	list<TableOP> list_tables;
	list<Lex> expr_update_list; //poliz
	list<Lex> expr_list; 		//poliz
	list<string> select_list;
	list<Lex> insert_list;
	list<FieldDef> create_list;
	list<string> const_str_list;
	list<long> const_num_list;
	char name_update[mlen_id + 1], str_like[mlen_id + 1];
	bool where_all, is_like, is_in, is_not, all_fields;
public:
	SQL_Request();
	~SQL_Request() = default;
	bool same_tables_or_fields();
	void clear_request();
	void cout_request();
	void cls_Tables(); // is_open = false for all tables
};
