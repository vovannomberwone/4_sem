#include "TableOP.h"
#include "SQL_Request.h"
#include <stack>
#include <list>
#include "Lex.h"
#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>

using namespace std;

SQL_Request::SQL_Request() {
	where_all = FALSE;
	all_fields = FALSE;
	is_like = FALSE;
	is_in = FALSE;
	is_not = FALSE;
}

void SQL_Request::cls_Tables() {
	for(auto i = list_tables.begin(); i != list_tables.end(); i++) {
		i->is_open = FALSE;
	}
	return;
}

void SQL_Request::clear_request() {
	type = EMPTY_REQUEST;
	for(auto i = list_tables.begin(); i != list_tables.end(); i++) {
		if (i->is_open) {
			i->Close_Table((*i).get_handle());
		}
		i->is_open = FALSE;
	}
	list_tables.clear();
	expr_list.clear();
	expr_update_list.clear();
	select_list.clear();
	insert_list.clear();
	create_list.clear();
	memset(str_like, 0, sizeof(str_like));
	memset(name_update, 0, sizeof(name_update));
	const_str_list.clear();
	const_num_list.clear();
	all_fields = FALSE;
	where_all = FALSE;
	is_like = FALSE;
	is_in = FALSE;
	is_not = FALSE;
	return;
}


bool SQL_Request::same_tables_or_fields() {
	vector<string> tnames;
	vector<string> fnames;
	char *fname;
	unsigned long sz;

	for (auto i = list_tables.begin(); i != list_tables.end(); i++) {
		tnames.push_back(i->get_table_name());
		for (unsigned int j = 0; j < i->ct_field; j++) {
			i->Get_Field_Name(i->get_handle(), j, &fname);
			fnames.push_back(string(fname));
		}
	}
	select_list.sort();
	sz = select_list.size();
	select_list.unique();
	if (select_list.size() < sz) {
		return TRUE;
	}
	sort(tnames.begin(), tnames.end());
	sz = tnames.size();
	tnames.erase(unique(tnames.begin(), tnames.end()), tnames.end());
	if (tnames.size() < sz) {
		return TRUE; 
	}
	sort(fnames.begin(), fnames.end());
	sz = fnames.size();
	tnames.erase(unique(fnames.begin(), fnames.end()), fnames.end());
	if (fnames.size() < sz) {
		return TRUE; 
	}
	return FALSE;
}

void SQL_Request::cout_request() {

	cout << "type rqst= " << type << endl;
	cout << "TABLES::" << endl;
	for (auto i = list_tables.begin(); i != list_tables.end(); i++) {
		cout << (i->get_table_name()) << endl;
	}
	cout << "expr_update_list::" << endl;
	for (auto i = expr_update_list.begin(); i != expr_update_list.end(); i++) {
		cout << *i << endl;
	}
	cout << "expr_list::" << endl;
	for (auto i = expr_list.begin(); i != expr_list.end(); i++) {
		cout << *i << endl;
	}
	cout << "select_list::" << endl;
	for (auto i = select_list.begin(); i != select_list.end(); i++) {
		cout << *i << endl;
	}
	cout << "insert_list::" << endl;
	for (auto i = insert_list.begin(); i != insert_list.end(); i++) {
		cout << *i << endl;
	}
	cout << "create_list::" << endl;
	for (auto i = create_list.begin(); i != create_list.end(); i++) {
		cout << i->name <<" , " << i->len << endl;
	}
	cout << "const_str_list::" << endl;
	for (auto i = const_str_list.begin(); i != const_str_list.end(); i++) {
		cout << *i << endl;
	}
	cout << "const_num_list::" << endl;
	for (auto i = const_num_list.begin(); i != const_num_list.end(); i++) {
		cout << *i << endl;
	}
	cout << "name_update:: " << name_update << endl;
	cout << "str_like:: " << str_like << endl;
	cout << "where_all " << where_all << endl;
	cout << "all_fields" << all_fields << endl;
	cout << "is_like " << is_like << endl;
	cout << "is_in " << is_in << endl;
	cout << "is_not " << is_not << endl;
	return;
}