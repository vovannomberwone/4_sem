#pragma once

#include "Error.h"

class Err_Lex: public Error {
public:
	Err_Lex() = default;
	Err_Lex(const char* s);
	~Err_Lex() = default;
	Err_Lex(const Err_Lex &e);
	Err_Lex& operator = (const Err_Lex &e);
	void cout_str();
};