#pragma once

#include "Lex.h"
#include "Table.h"
#include "Err_Table.h"
#include <list>

using namespace std;

class TableOP {
private:
	friend class SQL_Request; // access to field types
	friend class Synt_Scanner; // access to field types
	friend class SQL_Executer; // access to field types

	static const int mlen_name = 80;
	char table_name[mlen_name + 1];
	THandle table_handle;
	bool is_open;
	unsigned int ct_rec, ct_field;
public:
	TableOP();
	~TableOP();
	void Get_Info(const char* name, const bool &is_create_rqst);
	const THandle get_handle() const;
	const char* get_table_name();
	void cout_table();

	void Create_Table(const char *tableName, struct TableStruct *tableStruct);
	void Delete_Table(const char *tableName);
	void Open_Table(const char *tableName, THandle *tableHandle);
	void Close_Table(THandle tableHandle);
	void Move_First(THandle tableHandle);
	void Move_Last(THandle tableHandle);
	void Move_Next(THandle tableHandle);
	void Move_Previos(THandle tableHandle);
	bool Before_First(THandle tableHandle);
	bool After_Last(THandle tableHandle);
	void Get_Text(const THandle tableHandle, const char *fieldName, char **pvalue) const ;
	void Get_Long(const THandle tableHandle, const char *fieldName, long *pvalue) const ;
	void Start_Edit(THandle tableHandle);
	void Put_Text(THandle tableHandle, const char *fieldName, const char *value);
	void Put_Long(THandle tableHandle, const char *fieldName, const long value);
	void Finish_Edit(THandle tableHandle);
	void Create_New(THandle tableHandle);
	void Put_Text_New(THandle tableHandle, const char *fieldName, const char *value);
	void Put_Long_New(THandle tableHandle, const char *fieldName, const long value);
	void Insert_New(THandle tableHandle);
	void Inserta_New(THandle tableHandle);
	void Insertz_New(THandle tableHandle);
	void Delete_Rec(THandle tableHandle);
	const char* Get_Error_String(enum Errors code);
	void Get_Field_Len(THandle tableHandle, char*fieldName, unsigned *plen);
	void Get_Field_Type(THandle tableHandle, const char *fieldName, enum FieldType *ptype);
	void Get_Fields_Num(THandle tableHandle, unsigned *pNum);
	void Get_Field_Name(THandle tableHandle, unsigned index, char **pFieldName);
};