#pragma once

#include <stack>
#include "SQL_Executer.h"
#include "Err_Executer.h"
#include "Synt_Scanner.h"
#include "SQL_Request.h"

enum type_operation {
 	LOGIC, COMPARSION, ARIFMETIC
};

class SQL_Executer {
private:
	friend class SQL_Server;

	static const int mlen_id = 100;
	char table_name[mlen_id];
	Synt_Scanner s_scanner;
	void Ex_Select();
 	void Ex_Insert();
 	void Ex_Update();
 	void Ex_Delete();
 	void Ex_Create();
 	void Ex_Drop();
 	void get_operands(stack<Lex> &stack, Lex &op1, Lex &op2, const type_operation &t) const ;
 	void Expression_ID_STR(const SQL_Request &rqst, const Lex &l, const list<Lex> &expr_list, stack<Lex> &expr_stack) const ;
 	void Expression_ID_NUM(const SQL_Request &rqst, const Lex &l, const list<Lex> &expr_list, stack<Lex> &expr_stack) const ;
 	bool Logic_Expression(const SQL_Request &rqst);
 	const Lex Expression(const SQL_Request &rqst, const list<Lex> &expr_list, bool where_expression) const ;
 	int like_strcmp(const char *s1, const char *s2); // s2 - str_like
public:
	SQL_Executer(FILE *fp);
 	~SQL_Executer();
 	const char* get_table_name();
 	bool Execute();
 };