#pragma once

#include "Error.h"

class Err_Synt: public Error {
public:
	Err_Synt() = default;
	~Err_Synt() = default;
	Err_Synt(const char* s);
	Err_Synt(const Err_Synt &e);
	Err_Synt& operator = (const Err_Synt &e);
	void cout_str();
};