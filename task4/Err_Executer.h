#pragma once

#include "Error.h"

class Err_Executer: public Error {
public:
	Err_Executer() = default;
	Err_Executer(const char* s);
	~Err_Executer() = default;
	Err_Executer(const Err_Executer &e);
	Err_Executer& operator = (const Err_Executer &e);
	void cout_str();
};
