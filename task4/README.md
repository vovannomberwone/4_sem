Task4
Задание: интерпретатор модельного языка SQL.

Вариант:

Один клиент. Клиент и сервер на одной ЭВМ. Клиент и сервер должны указать порт.
Клиент получает от пользователя запрос на модельном языке SQL и, не анализируя  
его, передает серверу. Сервер анализирует запрос и в случае его корректности
выполняет запрос и передает клиенту таблицу, если тип запроса был SELECT.
Иначе сервер производит изменения с таблицами, расположенными на нем. 
Клиент сохраняет и распечатывает таблицу c именем sel_table_res. Если же запрос некорректен,
сервер передает клиенту сообщение об ошибке и завершает работу. Клиент выдает пользователю 
либо ответ на его запрос, либо сообщение об ошибке и завершает.  

Отличия от варианта задания сформулированного в файле model.sql.interpreter.2005.pdf:

В SELECT запросе могут участвовать несколько таблиц, но при этом не должны совпадать
имена полей ни в каких любых двух таблицах.
Также немного изменено выражение, смотри файл SQL_Changes1.jpg, SQL_Changes2.jpg, SQL_Changes3.jpg.

Ввод: клиент читает со стандартного потока ввода, при вводе,
концом запроса считается наличие '\n'. Завершение клиента или сервера происходит по Ctrl+C.

Запуск:

1) make Client
2) make Server
3) ./Server
4) ./Client

Удаление обьектных файлов:

make clean