#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <cstring>
#include "Err_Lex.h"
#include "Err_Table.h"
#include "Err_Synt.h"
#include "Err_Executer.h"

class SQL_Client {
private:
	static const int hostname_size = 64;
	static const int msg_size = 256;
	static const int port_min = 1024;
	static const int port_max = 65535;
	
	char c;
	int port, fd, pid;
	long len;
	struct sockaddr_in sin;
	char buf[msg_size];
	char hostname[hostname_size];
	struct hostent *hp;
public:
	SQL_Client();
	~SQL_Client() = default;;
	int msglen();
	int readmsg();
	void save_file();
};