#include "Err_Executer.h"
#include <iostream>

using namespace std;

Err_Executer::Err_Executer(const Err_Executer &e): Error(e) {
	;
}

Err_Executer::Err_Executer(const char* s) {
	putstr(s);
}

Err_Executer& Err_Executer::operator = (const Err_Executer &e) {
	putstr(e.getstr());
	return *this;
}

void Err_Executer::cout_str() {
	cout << "Error when try execute request:\n" << getstr() << endl;
	return;
}

