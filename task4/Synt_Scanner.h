#pragma once

#include "Lex_Scanner.h"
#include "SQL_Request.h"
#include "Err_Synt.h"
#include <stack>
#include "TableOP.h"

class Synt_Scanner {
private:
	static const int mlen_str = 150;

	friend class SQL_Executer;
	friend class SQL_Server;

	FieldDef cur_field;
	TableOP empty_table;
	SQL_Request rqst;
	Lex lexem;
	Lex_Scanner lscanner;
	char str[mlen_str];
	int spc_ct;
	type_of_lex c_type, sem_st_type;
	bool is_end, is_valid, is_update_expr, is_str_list;
	stack<type_of_lex> sem_stack;

	void gl();
	void miss();
	void det_lex(const char *s, const type_of_lex &t);
	void State_Select();
	void State_Insert();
	void State_Update();
	void State_Delete();
	void State_Create();
	void State_Drop();
	void sem_pop();
	void sem_push(const type_of_lex &t);
	void lex_add(const Lex &l); 
	void State_Where();
	void State_Expression();
	void State_A();
	void State_B();
	void State_C();
	void State_D();
	void State_E();
	void State_F();
	void check_types(const type_of_lex &op, const type_of_lex &a, const type_of_lex &b);
	void State_In();
	void State_Like();
public:
	Synt_Scanner(FILE *fp);
	~Synt_Scanner();
	bool str_valid();
	void scan_request(); // scan 1 request
	void scan_program(); // scan all program
	void scan_err_program(); // scan program with errors, error rqsts are missed with print error
	SQL_Request get_request();
};