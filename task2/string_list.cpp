#include <iostream>
#include <cstring>
#include "string_list.h"

using namespace std;

string_list::string_list() {
	str_len = 0;
	list_len = 0;
	next = NULL;
}

string_list::~string_list() {
	if (next != NULL) {
		delete next;
		next = NULL;	
	}
}

ostream& operator<< (ostream& os, const string_list &lst) {
	int i;
	string_list *temp = (string_list *)&lst;

	for (i = 0; i < lst.list_len; i++) {
		os << "list_len = " << temp->list_len << ' ' << "strlen = " << temp->str_len << ' ' << endl;
		os << "str = " << temp->str << "\nnext:" << endl;
		temp = temp->next;
	}
	os << "LIST IS EMPTY" << endl;
	return os;
}

const char string_list::error::errstring[ct_err_str][max_len] = {
	"Not error", "Index out of range", "Max leng of str was exceeded", "Incorrect attempt to make operator="
};

string_list::error::error(int i = 0) {
	error_type = i;
}

const char* string_list::error::getstr_error() {
	int temp;

	temp = error_type;
	error_type = 0;
	return errstring[temp];
}

string_list::string_list(const string_list &lst) {
	int i;
	class string_list *cur, *cur_lst;

	str_len = 0;
	list_len = 0;
	next = NULL;
	cur = this;
	cur_lst = (string_list*)&lst;
	for (i = 0; i < lst.list_len; i++) {
		strcpy(cur->str, cur_lst->str);
		cur->str_len = cur_lst->str_len;
		cur->list_len = cur_lst->list_len;
		cur = cur->next = new string_list();
		cur_lst = cur_lst->next;
	}
}

void string_list::copy_list(const string_list &lst) {
	int i;
	string_list *cur, *cur_lst;

	if (lst.list_len == 0) {
		str_len = 0;
		list_len = 0;
		next = NULL;
		return;
	}
	cur = this;
	cur_lst = (string_list*)&lst;
	for (i = 0; i < lst.list_len; i++) {
		strcpy(cur->str, cur_lst->str);
		cur->str_len = cur_lst->str_len;
		cur->list_len = cur_lst->list_len;
		cur = cur->next = new string_list();
		cur_lst = cur_lst->next;
	}
	return;
}

string_list& string_list::operator= (const string_list lst) {
	int i;
	class string_list *cur, *newlist;

	if (this == &lst) {									// ex.: a = a;
		return *this;
	}
	cur = this;
	for (i = 0; i < this->list_len + 1; i++) {			// ex.: a = a.next   
		if (cur == &lst) {
			break;
		}
		cur = cur->next;
	}
	if (cur == &lst) {
		newlist = new string_list(lst);
		(*this->next).~string_list();					// this->leng > lst.len >= 0
		(*this).copy_list(lst);
		delete newlist;
		return *this;
	}
	
	cur = (string_list*)&lst;
	for (i = 0; i < lst.list_len + 1; i++) {			// ex.: a.next = a 
		if (cur == this) {
			break;
		}
		cur = cur->next;
	}
	if (cur == this) {
		throw error(3);
	}

	if (next != NULL) {
		(*next).~string_list();
	}
	(*this).copy_list(lst);
	return *this;
}
	
string_list::string_list(const char *str) {
	if (strlen(str) >= max_len) {
		throw error(2);
	} else {
		strcpy(this->str, str);
		str_len = strlen(str);
		list_len = 1;
		next = new string_list();
	}
}

string_list string_list::operator+ (const string_list &lst) const {
	int i;
	string_list *cur, newlist(*this);
	
	cur = &newlist;
	while (cur->next != NULL) {
		cur = cur->next;
	}
	*cur = lst;
	cur = &newlist;
	for (i = lst.list_len + list_len; i > 0; i--) {
		cur->list_len = i;
		cur = cur->next;
	}
	return newlist;	
}

string_list& string_list::operator+= (const string_list &lst) {
	int i;
	string_list *cur;

	if (list_len == 0) {
		(*this) = lst;
		return *this;
	}
	cur = this;
	while (cur->next != NULL) {
		cur = cur->next;
	}
	*cur = lst;
	cur = this;
	for (i = lst.list_len + list_len; i > 0; i--) {
		cur->list_len = i;
		cur = cur->next;
	}
	return *this;
}
	
string_list operator+ (const char *str, string_list &lst) {
	string_list newlist(str);

	newlist.list_len = lst.list_len + 1;
	newlist.next = new string_list(lst);
	return newlist;
}

string_list operator+ (string_list &lst, const char *str) {
	string_list lastelem(str);

	string_list temp(lst + lastelem);
	return temp;
}

string_list operator- (string_list &lst, const char *str) {
	int i;
	string_list *cur, *newlist, *temp, empty_list;

	if (lst.list_len == 0) {
		return lst;
	}
	cur = &lst;
	for (i = 0; i < lst.list_len; i++) {
		if (strcmp(str, cur->str) == 0) {
			break;
		}
		(i < lst.list_len - 1) ? cur = cur->next : cur = cur;
	}
	if (i == (lst.list_len - 1) && strcmp(str, cur->str) != 0) {
		return lst;
	} else {	
		temp = &lst;
		for (i = 0; i < lst.list_len - cur->list_len; i++) {
			temp->list_len = temp->list_len - cur->list_len;
			temp = temp->next;
		}
		newlist = new string_list(*(cur->next));
		*cur = empty_list;
		lst += *newlist; 
		delete newlist;
		return lst; 
	}
}

char* string_list::operator[] (const int i) const {
	string_list *cur;
	int j;

	if (i >= list_len) {
		throw error(1);
	}
	cur = (string_list*)this;
	for (j = 0; j < i; j++) {
		cur = cur->next;
	}
	return cur->str;
}

/*
const char* string_list::operator[] (const int i) const {
	string_list *cur;
	int j;

	if (i >= list_len) {
		throw error(1);
	}
	cur = (string_list*)this;
	for (j = 0; j < i; j++) {
		cur = cur->next;
	}
	return cur->str;
}
*/

string_list::operator char*() {
	class string_list *cur;
	int i, all_len, len;
	char *all_str;

	if (list_len == 0) {
		all_str = new char[2];
		strcpy(all_str, "\n\0");
		return all_str;
	}
	all_len = 0;
	cur = this;
	for (i = 0; i < list_len; i++) {
		all_len += cur->str_len;
		cur = cur->next;
	}
	all_str = new char[all_len + list_len + 1];
	len = 0;
	cur = this;
	for (i = 0; i < list_len; i++) {
		strcpy(all_str + len, cur->str);
		all_str[len + cur->str_len] = '\n';
		len = len + cur->str_len + 1;
		cur = cur->next;
	}
	all_str[len] = '\0';
	return all_str;
}