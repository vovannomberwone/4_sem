#include <iostream>
#include "string_list.h"
#include <unistd.h>

using namespace std;

int main() {
	string_list obj1, obj2("str1");
	char *str;
	while (1) {
		try {
			cout << "obj2 = " << obj2;
			cout << "obj1 = " << obj1;
			obj1 = (obj1 + obj2);
			cout << "obj1 = " << obj1;
			obj2 = "str2";
			cout << "obj2 = " << obj2;
			obj1 += obj2;
			cout << "obj1 = " << obj1;
			obj1 = obj1 + "str3";
			cout << "obj1 = " << obj1;
			obj1 = obj1 - "str1";
			cout << "obj1 = " << obj1;
			obj1 = "str4" + obj1;
			cout << "obj1 = " << obj1;
			str = obj1;
			cout << "(char* str)obj1 = " << str << endl;
			delete []str;

			string_list obj3("newstr");
			obj3 = obj3 - "newstr";
			obj3 = obj3 - "newstr";
			//cout << obj1[1000] << endl;
		}
		catch (string_list::error err) {
			cout << err.getstr_error() << endl;
		}
	}
	pause();
	return 0;
}