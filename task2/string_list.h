#pragma once

#include <iostream>

using namespace std;

#define max_len 256
#define ct_err_str 4

class string_list {
private:
	char str[max_len];
	int str_len;
	int list_len;
	class string_list *next;
	void copy_list(const string_list &lst2); // this->leng = 0; 
public:
	string_list();
	~string_list();
	friend ostream& operator<< (ostream&, const string_list&); 
	class error {
	private:
		int error_type;
		int str_ct;
	public:
		static const char errstring[ct_err_str][max_len];
		//static const char errstring;
		error(int);
		const char* getstr_error();
	};
	string_list(const string_list&);
	string_list& operator= (const string_list);
	string_list(const char *);
	string_list operator+ (const string_list&) const;
	string_list& operator+= (const string_list&);
	friend string_list operator+ (const char *, string_list&);
	friend string_list operator+ (string_list&, const char *);
	friend string_list operator- (string_list&, const char *);
	char* operator[] (const int) const;
	//const char* operator[] (const int) const;
	operator char*();
};
