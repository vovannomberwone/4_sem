#include "cube_without_sphere.h"

double cube_without_sphere::volume() {
	return this->cube::volume() - this->sphere::volume();
}