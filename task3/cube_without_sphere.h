#pragma once

#include "cube.h"
#include "sphere.h"

class cube_without_sphere: public virtual cube, public virtual sphere {
public:
	cube_without_sphere(double radius = 1, double a = 2): cube(a), sphere(radius) {
	if (getradius() > geta()) {
			throw "incorrect figure cube_without_sphere: radius > a";
		}
	}
	~cube_without_sphere() {
		;
	}
	double volume();	
};