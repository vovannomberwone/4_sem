#include "sphere.h"

sphere::sphere(double radius) {
	r = radius;
}

double sphere::volume() {
	return 4 * pi * pow(r, 3) / 3;
}
double sphere::getradius() {
	return r;
}