#pragma once

#include "figure.h"

class parallelogram: public figure {
private:
	double a, b, c;
public:
	parallelogram(double x = 1, double y = 1, double z = 1);
	~parallelogram() {
		;
	}
	double geta();
	double volume();	
};