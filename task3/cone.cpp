#include "cone.h" 

cone::cone(double radius, double height) {
	r = radius;
	h = height;
}

double cone::volume() {
	return pi * r * r * h / 3;
}