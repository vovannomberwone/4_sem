#pragma once

#include "figure.h"

class cone: public figure {
private:
	double r, h;
public:
	cone(double radius = 1, double height = 1);
	~cone() {
		;
	}
	double volume();
};