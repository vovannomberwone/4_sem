#include "cylinder.h"

cylinder::cylinder(double radius, double height) {
	r = radius;
	h = height;
}

double cylinder::volume() {
	return pi * r * r * h;
}