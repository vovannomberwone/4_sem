#pragma once

#include "figure.h"

class cylinder: public figure {
private:
	double r, h;
public:
	cylinder(double radius = 1, double height = 1);
	~cylinder() {
		;
	}
	double volume();
};
