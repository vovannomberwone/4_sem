#include <iostream>
#include <cmath>
#include "sphere.h"
#include "cylinder.h"
#include "parallelogram.h"
#include "cone.h"
#include "cube.h"
#include "cube_without_sphere.h"

using namespace std;

int main() {
	int i;
	double total_volume = 0;
	figure* arr[6];
	parallelogram f1; // volume = 1
	sphere f2(pow(1 / pi, 1.0 / 3)); // volume = 4 / 3
	cylinder f3(1, 1 / pi); // volume = 1
	cone f4(1, 1 / pi); // volume = 1 / 3
	cube f5;	// volume = 1
	cube_without_sphere f6; // volume = 8 - 4 * pi/ 3 ~4
	arr[0] = &f1;
	arr[1] = &f2;
	arr[2] = &f3;
	arr[3] = &f4;
	arr[4] = &f5;
	arr[5] = (figure*)(sphere*)(&f6);
	for (i = 0; i < 6; i++) {
		total_volume += arr[i]->volume();
	}
	cout << total_volume << endl; // 8.47
	return 0;
} 