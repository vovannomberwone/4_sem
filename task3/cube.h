#pragma once

#include "parallelogram.h"

class cube: public parallelogram {
public:
	cube(double a = 1) : parallelogram(a, a, a) {
		;
	}
	~cube() {
		;
	}
	double volume();
};