#pragma once

#include "figure.h"

class sphere: public figure {
private:
	double r;
public:
	sphere(double radius = 1);
	~sphere() {
		;
	}
	double getradius();
	double volume();
};