#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#define name_size 30
#define hostname_size 64
#define msg_size 256

int fd;

int msglen(char *buf) {
	int len = 0;

	while (buf[len] != '\n') {
		len++;
	}
	return len;
}

int readmsg(char *buf, int max_size) {
	int len = 0;
	char c;

	for ( ; len < max_size; ) {
		read(0, &c, 1);
		if (c == ' ') {
			continue;
		} else {
			buf[0] = c; 
			len = 1;
			if (c == '\n') {
				return -1;
			} else {
				break;
			}
		}
	}
	for ( ; len < max_size; len++) {
		read(0, &c, 1);
		buf[len] = c;
		if (c == '\n') {
			buf[len] = '\0';
			return 0;
		}
		if (len == max_size - 1) {
			return -1;
		}
	}
	return 0;
}

void handler(int sig) {
	if (sig == SIGUSR1) {
		shutdown(fd, 2);
		close(fd);
		kill(SIGINT, getpid());
	}
	return;
}

int main() {
	int port, isnt_valid = 1, len, pid;
	const int port_min = 1024, port_max = 65535;
	struct sockaddr_in sin;
	const char str1[] = "Valid Name";
	const char str2[] = "Max count of clients is reached, try again later\n";
	const char str[] = ": ";
	char name[name_size];
	char buf[msg_size], msgbuf[msg_size + name_size + strlen(str) + 1];
	char hostname[hostname_size];
	struct hostent *hp;

	signal(SIGUSR1, handler);
	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("client: socket");
		exit(1);
	}
	sin.sin_family = AF_INET;
	printf("Enter port\n");
	scanf("%d", &port);
	while (!(port >= port_min && port <= port_max)) {
		printf("Enter port from %d to %d\n", port_min, port_max);
		scanf("%d", &port);
	}
	getchar();														// miss '\n' after port
	sin.sin_port = htons(port);
	gethostname(hostname, sizeof(hostname));
	if ((hp = gethostbyname(hostname)) == NULL) {
		fprintf(stderr, "%s: unknown host\n", hostname);
		return 1;
	}
	memcpy(&sin.sin_addr, hp->h_addr, hp->h_length);
 	if (connect(fd, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
 		perror("client: connect");
 		return 1;
 	}
 	printf("Enter nickname:\n");
 	if (readmsg(name, name_size) < 0) {
 		printf("Invalid name, try again later\n");
 		return 1;
 	}
 	len = strlen(name);
 	name[len] = '\n';
 	len++;
 	write(fd, &len, sizeof(int));
 	write(fd, name, len);
 	while (isnt_valid) {
	 	read(fd, &len, sizeof(int));
	 	read(fd, buf, len);
	 	buf[len - 1] = '\0';
 		if (strcmp(buf, str1) == 0) {
 			isnt_valid = 0;
 		} else {
 			if (strcmp(buf, str2) == 0) {
 				printf("%s\n", buf);
 				return 0;
 			}
 			printf("%s\n", buf);
 			if (readmsg(name, name_size) < 0) {
		 		printf("Invalid name, try again later\n");
		 		return 1;
		 	}
 			len = strlen(name);
 			name[len] = '\n';
 			len++;
 			write(fd, &len, sizeof(int));
 			write(fd, name, len);
 		}
 	}
 	name[msglen(name)] = '\0';
 	if ((pid = fork()) > 0) {
 		while (1) {
			if (read(fd, &len, sizeof(int)) <= 0) {
				shutdown(fd, 2);
				close(fd);
				kill(SIGUSR1, pid);
				return 0;
			}
			if (read(fd, msgbuf, len) <= 0) {
				shutdown(fd, 2);
				close(fd);
				kill(SIGUSR1, pid);
				return 0;
			}
			write(1, msgbuf, len);
		}
	} else {
		while (1) {
			printf("%s: ", name);
			fflush(stdout);
			if (readmsg(buf, msg_size) < 0) {
 				printf("Invalid name, try again later\n");
 				shutdown(fd, 2);
 				close(fd);
 				kill(SIGUSR1, getpid());
 				return 1;
 			}
			len = strlen(buf);
			buf[len] = '\n';
			len++;
			write(fd, &len, sizeof(int));
			write(fd, buf, len);
		}
	}
	shutdown(fd, 2);
 	close(fd);
 	return 0;
}