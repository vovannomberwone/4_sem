#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define rqst_max 5
#define name_size 30
#define msg_size 256
#define client_max 30
#define hostname_size 64

int msglen(char *buf) {
	int len = 0;

	while (buf[len] != '\n') {
		len++;
	}
	return len;
}

void send_all(int *fds, int pos, char *buf, int ct) {
	int i = 0, len;

	while (i < ct) {
		if (i == pos) {
			i++;
			continue;
		}
		len = msglen(buf) + 1;
		write(fds[i], &len, sizeof(int));
		write(fds[i], buf, len);
		i++;
	}
	return;
}

void add_into_arr(int *fds, int fd, int *ct, char names[client_max][name_size]) {
	int j, is_valid, len;
	char buf[msg_size];
	const char str1[] = "Invalid Name, try again\n";
	const char str2[] = "Valid Name\n";
	const char str3[] = " Joined chat\n";
	const char str4[] = "Max count of clients is reached, try again later\n";

	if ((*ct) == (client_max - 1)) {
		len = strlen(str4);
		write(fd, &len, sizeof(int));
		write(fd, str4, len);
		close(fd);
		return;	
	}
	fds[*ct] = fd;
	while (1) {
		is_valid = 1;
		if (read(fd, &len, sizeof(int)) <= 0) {
			return;
		}
		if (read(fd, names[*ct], len) <= 0) {
			return;
		}
		names[*ct][len - 1] = '\0';	
		for (j = 0; j < *ct; j++) {
			if (strcmp(names[j], names[*ct]) == 0) {
				len = strlen(str1);
				write(fds[*ct], &len, sizeof(int));
				write(fds[*ct], str1, len);
				is_valid = 0;
				break;
			}
		}
		if (is_valid) {
			len = strlen(str2);
			write(fds[*ct], &len, sizeof(int));
			write(fds[*ct], str2, len);
			break;
		}
	}
	strcpy(buf, names[*ct]);
	strcpy(buf + strlen(names[*ct]), str3);
	len = strlen(names[*ct]) + strlen(str3);
	send_all(fds, *ct, buf, *ct);
	printf("%s has connected\n", names[*ct]);
	(*ct)++;
	return;
}

void del_from_arr(int *fds, int pos, int *ct, char names[client_max][name_size]) {
	int k;
	char buf[msg_size];
	char str[] = " Has left chat\n";

	shutdown(fds[pos], 2);
	close(fds[pos]);
	strcpy(buf, names[pos]);
	strcpy(buf + strlen(names[pos]), str);
	send_all(fds, pos, buf, *ct); 
	printf("%s has disconnected\n", names[pos]);
	for (k = pos; k < *ct - 1; k++) {
		fds[k] = fds[k + 1];
		strcpy(names[k], names[k + 1]);
	}
	(*ct)--;
	return;
}

int main() {
	int ct, max_d, sd, sd_new, fromlen, port, i, len, opt = 1;
	int fds[client_max];
	const int port_min = 1024, port_max = 65535;
	const char str[] = ": ";
	char buf[msg_size], msgbuf[msg_size + name_size + strlen(str) + 1];
	char hostname[hostname_size];
	char names[client_max][name_size];
	struct hostent *hp;
	struct sockaddr_in sin, fromsin;
	fd_set readfds;

	if ((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0) { 
		perror("server: socket"); 
		return 1;
	}
	printf("Enter port\n");
	scanf("%d", &port);
	while (!(port >= port_min && port <= port_max)) {
		printf("Enter port from %d to %d\n", port_min, port_max);
		scanf("%d", &port);
	}
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	gethostname(hostname, sizeof(hostname));
	if ((hp = gethostbyname(hostname)) == NULL) {
		fprintf(stderr, "%s: unknown host\n", hostname);
		return 1;
	}
	memcpy(&sin.sin_addr, hp->h_addr, hp->h_length);
	setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
	if (bind(sd, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
		perror("server: bind");
		return 1;
	}
	if (listen(sd, rqst_max) < 0) {
		perror("server: listen");
		return 1;
	}
	ct = 0;
	while (1) {
		max_d = sd;
		FD_ZERO(&readfds);
		FD_SET(sd, &readfds);
		for (i = 0; i < ct; i++) {
			FD_SET(fds[i], &readfds);
			if (fds[i] > max_d) {
				max_d = fds[i];
			}
		}
		if (select(max_d + 1, &readfds, NULL, NULL, NULL) < 1) {
			perror("server: select");
			return 1;
		}
		if (FD_ISSET(sd, &readfds)) {
			fromlen = sizeof(fromsin);
			if ((sd_new = accept(sd, (struct sockaddr *)&fromsin, &fromlen)) < 0) {
				perror("server: accept");
				return 1;
			}
			add_into_arr(fds, sd_new, &ct, names);
		}
		for (i = 0; i < ct; i++) {
			if (FD_ISSET(fds[i], &readfds)) {
				if (read(fds[i], &len, sizeof(int)) <= 0) {
					del_from_arr(fds, i, &ct, names);
				} else {
					if (read(fds[i], buf, len) <= 0) {
						del_from_arr(fds, i, &ct, names);
						continue;
					}
					buf[len - 1] = '\0';
					strcpy(msgbuf, names[i]);
					strcpy(msgbuf + strlen(names[i]), str);
					strcpy(msgbuf + strlen(names[i]) + strlen(str), buf);
					msgbuf[strlen(names[i]) + strlen(str) + strlen(buf)] = '\n';
					send_all(fds, i, msgbuf, ct);
				}
			}
		}
	}
	close(sd);
	return 0;
}